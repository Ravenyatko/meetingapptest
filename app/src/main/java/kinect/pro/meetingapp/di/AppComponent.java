package kinect.pro.meetingapp.di;


import javax.inject.Singleton;

import dagger.Component;
import kinect.pro.meetingapp.activity.CalendarActivity;
import kinect.pro.meetingapp.activity.meeting.ChangeDateActivity;
import kinect.pro.meetingapp.activity.meeting.CreateGroupActivity;
import kinect.pro.meetingapp.activity.meeting.CreateMeetingActivity;
import kinect.pro.meetingapp.activity.meeting.InfoMeetingActivity;
import kinect.pro.meetingapp.activity.meeting.PickContactActivity;
import kinect.pro.meetingapp.activity.user.LoginActivity;
import kinect.pro.meetingapp.activity.user.ProfileDetailsActivity;
import kinect.pro.meetingapp.popup.PopupReceiver;
import kinect.pro.meetingapp.rest.NetModule;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface AppComponent {

    void inject(LoginActivity activity);

    void inject(CreateMeetingActivity activity);

    void inject(CalendarActivity activity);

    void inject(InfoMeetingActivity activity);

    void inject(PopupReceiver receiver);

    void inject(ProfileDetailsActivity activity);

    void inject(PickContactActivity activity);

    void inject(CreateGroupActivity activity);

    void inject(ChangeDateActivity activity);
}