package kinect.pro.meetingapp.view;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.WeekDayTimeModel;

/**
 * Created on 30.11.2017.
 */

public class DayTimePickerView extends LinearLayout implements TimePickerDialog.OnTimeSetListener, AdapterView.OnItemSelectedListener {

    private final static int PICK_START = 1;
    private final static int PICK_END = 2;

    @BindView(R.id.day_spinner)
    Spinner mDaySpinner;
    @BindView(R.id.from_et)
    EditText mStartEt;
    @BindView(R.id.to_et)
    EditText mEndEt;

    private int currentPick = -1;

    private Calendar startCalendar;
    private Calendar endCalendar;

    public DayTimePickerView(Context context) {
        super(context);
        init(context);
    }

    public DayTimePickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DayTimePickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DayTimePickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.layout_daytime_pick, this);
        ButterKnife.bind(this, this);
        mDaySpinner.setOnItemSelectedListener(this);
    }

    public void setCurrentDay(int day) {
        if (day < 1 || day > 7) {
            return;
        }
        mDaySpinner.setOnItemSelectedListener(null);
        mDaySpinner.setSelection(day - 1);
        mDaySpinner.setOnItemSelectedListener(this);
    }

    @OnClick(R.id.clear_btn)
    void removeView() {
        ViewGroup parent = (ViewGroup) getParent();
        if (parent != null) {
            parent.removeView(this);
        }
    }


    @OnClick(R.id.from_et)
    void pickStartTime() {
        currentPick = PICK_START;
        if (getContext() instanceof AppCompatActivity) {
            MeetingTimePicker meetingTimePicker = new MeetingTimePicker();
            meetingTimePicker.setListener(this);
            meetingTimePicker.show(((AppCompatActivity) getContext()).getSupportFragmentManager(), "StartTimePicker");
        }
    }

    @OnClick(R.id.to_et)
    void pickEndTime() {
        currentPick = PICK_END;
        if (getContext() instanceof AppCompatActivity) {
            MeetingTimePicker meetingTimePicker = new MeetingTimePicker();
            meetingTimePicker.setListener(this);
            meetingTimePicker.show(((AppCompatActivity) getContext()).getSupportFragmentManager(), "EndTimePicker");
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        switch (currentPick) {
            case PICK_START:
                mStartEt.setError(null);
                startCalendar = Calendar.getInstance();
                startCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                startCalendar.set(Calendar.MINUTE, minute);
                mStartEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                break;
            case PICK_END:
                mEndEt.setError(null);
                endCalendar = Calendar.getInstance();
                endCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                endCalendar.set(Calendar.MINUTE, minute);
                mEndEt.setText(String.format(Locale.ENGLISH, "%02d:%02d", hourOfDay, minute));
                break;
        }
        currentPick = -1;
    }

    public WeekDayTimeModel getData() {
        return new WeekDayTimeModel(mDaySpinner.getSelectedItemPosition() + 1,
                mStartEt.getText().toString(),
        mEndEt.getText().toString());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public boolean validate() {
        WeekDayTimeModel weekDayTimeModel = getData();
        boolean result = true;
        if (weekDayTimeModel.getStartTime().isEmpty()) {
            mStartEt.setError(getContext().getString(R.string.fill_start_error));
            result = false;
        }
        if (weekDayTimeModel.getEndTime().isEmpty()) {
            mEndEt.setError(getContext().getString(R.string.fill_end_error));
            result = false;
        }
        if (startCalendar != null && endCalendar != null
                && endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis() <= 5 * 60 * 1000) {
            Toast.makeText(getContext(), R.string.error_min_time, Toast.LENGTH_LONG).show();
            result = false;
        }

        return result;
    }

}
