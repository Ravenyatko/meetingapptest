package kinect.pro.meetingapp.firebase;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import dagger.Module;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.Profile;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.other.Constants;

import static kinect.pro.meetingapp.other.Constants.DEFAULT;
import static kinect.pro.meetingapp.other.Constants.KEY_PHONE;
import static kinect.pro.meetingapp.other.Constants.KEY_PROVIDER_ID;
import static kinect.pro.meetingapp.other.Constants.KEY_UID;
import static kinect.pro.meetingapp.other.Constants.URL_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_GROUPS;
import static kinect.pro.meetingapp.other.Constants.URL_MEETING;
import static kinect.pro.meetingapp.other.Constants.URL_PROFILE;


@Module
public class DatabaseManager {

    private static final String TAG = DatabaseManager.class.getSimpleName();
    private ArrayList<MeetingModel> mListMeetingModels;
    private ArrayList<UserGroup> mListUserGroups;
    private ArrayList<String> mListMeetups;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mMeetingReference;
    private DatabaseReference mContantsReference;
    private DatabaseReference mGroupsReference;
    private SharedPreferences mPreference;
    private OnDatabaseDataChanged mDatabaseListener;

    private Profile mProfile;

    private DatabaseReference mProfileReference;

    @Inject
    public DatabaseManager(SharedPreferences sharedPreferences) {
        mDatabase = FirebaseDatabase.getInstance();
        mListMeetups = new ArrayList<>();
        mListMeetingModels = new ArrayList<>();
        mListUserGroups = new ArrayList<>();
        mPreference = sharedPreferences;
        mMeetingReference = mDatabase.getReference(URL_MEETING);
        mContantsReference = mDatabase.getReference(URL_CONTACTS);
        mGroupsReference = mDatabase.getReference(URL_GROUPS);
//        mPreference.edit().putString(KEY_PHONE, "+48737836760").commit();
    }

    public void setDatabaseManagerListener(OnDatabaseDataChanged listener) {
        mDatabaseListener = listener;
    }

    public void initProfile() {
        String currentUser = mPreference.getString(KEY_PHONE, "");
        if (currentUser.isEmpty()) {
            return;
        }
        mProfileReference = mDatabase.getReference(URL_PROFILE + currentUser);
        mProfileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProfile = dataSnapshot.getValue(Profile.class);

                if (mDatabaseListener != null) {
                    mDatabaseListener.onDataChanged(URL_PROFILE, dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (mDatabaseListener != null) {
                    mDatabaseListener.onCancelled(databaseError);
                }
            }
        });

        mProfileReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProfile = dataSnapshot.getValue(Profile.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
    }

    public void initMeeting() {
        mMeetingReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange");
                mListMeetingModels.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    MeetingModel meetingModel = noteDataSnapshot.getValue(MeetingModel.class);
                    if (meetingModel != null) {
                        mListMeetups.clear();
                        for (int i = 0; i < meetingModel.getParticipants().size(); i++) {
                            if (meetingModel.getParticipants().get(i) != null && meetingModel.getParticipants().get(i).getPhone()
                                    .equals(mPreference.getString(KEY_PHONE, DEFAULT))) {
                                if (!mListMeetups.contains(meetingModel.getTopic())) {
                                    mListMeetingModels.add(meetingModel);
                                    mListMeetups.add(meetingModel.getTopic());
                                    Log.d(TAG, "Update mDatabase");
                                }
                            }
                        }
                    }
                }
                Collections.sort(mListMeetingModels, new Comparator<MeetingModel>() {
                    @Override
                    public int compare(MeetingModel o1, MeetingModel o2) {
                        Long start = o1.getDate();
                        Long start2 = o2.getDate();
                        return start2.compareTo(start);
                    }
                });
                if (mDatabaseListener != null) {
                    mDatabaseListener.onDataChanged(URL_MEETING, dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, error.getMessage());
                if (mDatabaseListener != null) {
                    mDatabaseListener.onCancelled(error);
                }
            }
        });
    }

    public void initGroups() {
        String currentUser = mPreference.getString(KEY_PHONE, "");
        if (currentUser.isEmpty()) {
            return;
        }
        mGroupsReference = mDatabase.getReference(URL_GROUPS + currentUser + "/");
        mGroupsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange");
                mListUserGroups.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    UserGroup group = noteDataSnapshot.getValue(UserGroup.class);
                    if (group != null) {
                        mListUserGroups.add(group);
                    }
                }
                if (mDatabaseListener != null) {
                    mDatabaseListener.onDataChanged(URL_GROUPS, dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, error.getMessage());
                if (mDatabaseListener != null) {
                    mDatabaseListener.onCancelled(error);
                }
            }
        });
    }

    public void saveCurrentUserDetails() {
        if (mPreference.contains(KEY_PHONE) &&
                mPreference.contains(Constants.KEY_PROVIDER_ID) &&
                mPreference.contains(Constants.KEY_UID)) {

            ContactsModels models = new ContactsModels();
            models.setPhone(mPreference.getString(KEY_PHONE, DEFAULT));
            models.setProviderId(mPreference.getString(KEY_PROVIDER_ID, DEFAULT));
            models.setUid(mPreference.getString(KEY_UID, DEFAULT));
            DatabaseReference variableReference = mDatabase.getReference(URL_CONTACTS + mPreference.getString(KEY_PHONE, DEFAULT));
            variableReference.setValue(models);
        }
    }

    public void subscribeToContactsUpdates(OnDatabaseDataChanged listener) {
        mContantsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (listener != null) {
                    listener.onDataChanged(URL_CONTACTS, dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
                if (listener != null) {
                    listener.onCancelled(error);
                }
            }
        });
    }

    public void addOrUpdateMeeting(MeetingModel meetingModel, String nameMeeting) {
        DatabaseReference databaseReference = mDatabase.getReference(URL_MEETING + nameMeeting);
        databaseReference.setValue(meetingModel);
    }

    public void createNewGroup(String groupName, UserGroup group) {
        String currentUser = mPreference.getString(KEY_PHONE, "");
        if (currentUser.isEmpty()) {
            return;
        }
        DatabaseReference databaseReference = mDatabase.getReference(URL_GROUPS + currentUser + "/" + groupName);
        databaseReference.setValue(group);
    }

    public void saveProfile(Profile profile) {
        mProfileReference.setValue(profile);
    }

    public Profile getProfile() {
        return mProfile;
    }

    public DatabaseReference getCurrentMeetingReference(String topicUrl) {
        return mMeetingReference.child(topicUrl);
    }

    public ArrayList<MeetingModel> getMeetingModels() {
        return mListMeetingModels;
    }

    public ArrayList<UserGroup> getListUserGroups() {
        return mListUserGroups;
    }

    public interface OnDatabaseDataChanged {
        void onDataChanged(String url, DataSnapshot dataSnapshot);

        void onCancelled(DatabaseError error);
    }

}
