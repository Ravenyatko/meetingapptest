package kinect.pro.meetingapp.utils;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.model.WeekDayTimeModel;

import static kinect.pro.meetingapp.other.Constants.TIME_FORMAT;

/**
 * Created on 04.12.2017.
 */

public class TimeUtil {

    private static boolean isGroupMathesInterval(Interval meetingInterval, UserGroup group) {
        if (group.getAvailableTimes() == null) {
            return true; //true if group haven't specified intervals
        }
        Calendar timingCalendarStart = Calendar.getInstance();
        Calendar timingCalendarEnd = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH);

        try {
            int days = meetingInterval.toPeriod().getDays() + 1;
            for (int i = 0; i < days; i++) {
                DateTime dateTime = meetingInterval.getStart().plusDays(i);
                Interval meetingDayInterval;
                if (days == 1) { // one day meeting
                    meetingDayInterval = meetingInterval;
                } else {
                    if (i == 0) { // first meeting day
                        meetingDayInterval = new Interval(meetingInterval.getStart(), dateTime.plusDays(1).withTimeAtStartOfDay());
                    } else if (i == days - 1) {  // last meeting day
                        meetingDayInterval = new Interval(dateTime.withTimeAtStartOfDay(), meetingInterval.getEnd());
                    } else { // all other days
                        meetingDayInterval = new Interval(dateTime.withTimeAtStartOfDay(), dateTime.plusDays(1).withTimeAtStartOfDay());
                    }
                }
                meetingDayInterval = meetingDayInterval.withStart(meetingDayInterval.getStart().plusMinutes(1))
                        .withEnd(meetingDayInterval.getEnd().minusMinutes(1));
                boolean matchAtLeastOne = false;
                for (WeekDayTimeModel timing : group.getAvailableTimes()) {
                    timingCalendarStart.setTimeInMillis(simpleDateFormat.parse(timing.getStartTime()).getTime());
                    timingCalendarEnd.setTimeInMillis(simpleDateFormat.parse(timing.getEndTime()).getTime());

                    int hoursStart = timingCalendarStart.get(Calendar.HOUR_OF_DAY);
                    int minutesStart = timingCalendarStart.get(Calendar.MINUTE);
                    int hoursEnd = timingCalendarEnd.get(Calendar.HOUR_OF_DAY);
                    int minutesEnd = timingCalendarEnd.get(Calendar.MINUTE);
                    Interval timingInterval = new Interval(dateTime.withHourOfDay(hoursStart).withMinuteOfHour(minutesStart),
                            dateTime.withHourOfDay(hoursEnd).withMinuteOfHour(minutesEnd));
                    if (dateTime.getDayOfWeek() == timing.getDayOfWeek() && timingInterval.contains(meetingDayInterval)) {
                        matchAtLeastOne = true;
                    }
                }
                return matchAtLeastOne;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean isGroupsMatched(long startTime, long endTime, List<UserGroup> groups) {
        if (groups != null && groups.size() > 0) {
            for (UserGroup group : groups) {
                if (!isGroupMathesInterval(new Interval(startTime, endTime), group)) {
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean isMeetingsMatched(MeetingModel checkedMeeting, List<MeetingModel> meetings) {
        if (meetings != null && meetings.size() > 1) {
            meetings.remove(checkedMeeting); //no need to check current
            Interval currentInterval = new Interval(checkedMeeting.getDate(), checkedMeeting.getDuration());

            if (meetings.size() == 1) {
                return currentInterval.overlap(new Interval(meetings.get(0).getDate(), meetings.get(0).getDuration())) == null;
            }

            long[] timings = new long[]{checkedMeeting.getDate(), checkedMeeting.getDuration()};

            Collections.sort(meetings, new Comparator<MeetingModel>() {
                @Override
                public int compare(MeetingModel o1, MeetingModel o2) {
                    Long start1 = o1.getDate();
                    Long start2 = o2.getDate();
                    timings[0] = Math.min(timings[0], o1.getDate());
                    timings[1] = Math.max(timings[0], o1.getDuration());
                    return start1.compareTo(start2);
                }
            });

            List<Interval> filteredIntervals = new ArrayList<>();

            long minStartTime = timings[0];
            long maxEndTime = timings[1];

            if(checkedMeeting.getDuration() <= minStartTime || checkedMeeting.getDate() >= maxEndTime){
                return true;
            }

            for (int i = 0; i < meetings.size(); i++) {
                MeetingModel item = meetings.get(i);

                minStartTime = Math.min(minStartTime, item.getDate());
                maxEndTime = Math.max(maxEndTime, item.getDuration());

                if (!Collections.disjoint(item.getParticipants(), checkedMeeting.getParticipants())
                        && item.getDuration() >= checkedMeeting.getDate()) {
                    // we need to check only meetings with similar users and with end date >= our start date
                    filteredIntervals.add(new Interval(item.getDate(), item.getDuration()));
                }
            }
            minStartTime = new DateTime(minStartTime).withMillisOfSecond(0).withSecondOfMinute(0).getMillis();
            maxEndTime = new DateTime(maxEndTime).withMillisOfSecond(0).withSecondOfMinute(0).getMillis();
            if (checkedMeeting.getDuration() < minStartTime || checkedMeeting.getDate() > maxEndTime) {
                return true; // all is ok if our meeting not in ranges of current meetings
            } else {
                List<Interval> gaps = DateTimeGapFinder.findGaps(filteredIntervals, new Interval(minStartTime, maxEndTime));
                for (Interval gap : gaps) {
                    if (gap.contains(currentInterval)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return true;
        }
    }
}
