package kinect.pro.meetingapp.other;

public class Constants {

    public static final String ACCOUNT_SID = "ACdcc3c28facf1c33ad16c4c4326771692";
    public static final String AUTH_TOKEN = "9a4666ad2abcd9c484268279eac9036a";

    public static final String POPUP_RECEIVER_ACTION = "kinect.pro.meetingapp.POPUP_ACTION";
    public static final String POPUP_MEETING_URL = "metting_url";

    public static final String ACCOUNT_SID_KEY = "ACCOUNT_SID";
    public static final String AUTHORIZATION_KEY = "Authorization";

    public static final String DEFAULT_REGION = "EN";

    public static final String TWILLIO_ENDPOINT = "Accounts/{ACCOUNT_SID}/SMS/Messages";

    public static final String KEY_PHONE = "keyPhone";
    public static final String KEY_PROVIDER_ID = "keyProviderId";
    public static final String KEY_UID = "keyUID";
    public static final String KEY_INFO_EVENT = "keyInfoEvent";
    public static final String KEY_POPUP = "keyPopup";
    public static final String KEY_AVATAR = "profile_avatar";
    public static final String KEY_CONTACTS = "contacts";

    public static final String MEETING = "Meeting";
    public static final String DEFAULT = "default";

    public static final String URL_CONTACTS = "/Contacts/";
    public static final String URL_MEETING = "/Meeting/";
    public static final String URL_PROFILE = "/Profile/";
    public static final String URL_GROUPS = "/Groups/";

    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
    public static final String TIME_FORMAT = "HH:mm";

    public static final String STATUS_PENDING = "Pending";
    public static final String STATUS_CANCELED = "Canceled";
    public static final String STATUS_CONFIDMED = "Confirmed";

    public static final String BASE_URL = "https://api.twilio.com/2010-04-01/";
    public static final String NUMBER_FROM = "+14142402044";
    public static final int TYPE_CHRONOLOGY_VIEW = 1;
    public static final int TYPE_DAY_VIEW = 2;
    public static final int TYPE_MONTH_VIEW = 3;

    public static final int PLACE_PICKER_REQUEST = 1;

    public static final String POPUP_EXTRA = "notification";

    public static final String PROFILE_TYPE_BUSINESS = "business";
    public static final String PROFILE_TYPE_PRIVATE = "private";



}
