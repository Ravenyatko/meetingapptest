package kinect.pro.meetingapp.other;


import android.content.Context;
import android.net.ConnectivityManager;

import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.Participant;

import static kinect.pro.meetingapp.other.Constants.STATUS_CANCELED;
import static kinect.pro.meetingapp.other.Constants.STATUS_CONFIDMED;


public class Utils {

    public static boolean isNetworkOnline(Context context) {
        ConnectivityManager cManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cManager.getActiveNetworkInfo() != null;
    }


    public static int getMeetingColor(MeetingModel meetingModel) {
        boolean isAllConfirmed = true;
        for (Participant participant : meetingModel.getParticipants()) {
            if(participant == null){
                continue;
            }
            if (!participant.getStatus().equals(Constants.STATUS_CONFIDMED)) {
                isAllConfirmed = false;
                if (participant.getStatus().equals(Constants.STATUS_CANCELED)) {
                    return R.color.colorDarksalmon;
                }
            }
        }
        if (isAllConfirmed) {
            return R.color.colorSilver;
        } else {
            return R.color.colorKhaki;
        }
    }

    public static String buildParticipantsString(MeetingModel meetingModel) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < meetingModel.getParticipants().size(); i++) {
            Participant participant = meetingModel.getParticipants().get(i);
            String label = (participant.getName() == null || participant.getName().isEmpty()) ? participant.getPhone() : participant.getName();
            stringBuilder.append(label)
                    .append(" - ").append(meetingModel.getParticipants().get(i).getStatus())
                    .append("\n");
        }
        return stringBuilder.toString();
    }
}
