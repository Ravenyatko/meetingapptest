package kinect.pro.meetingapp.activity.meeting;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.adapter.GroupContactsAdapter;
import kinect.pro.meetingapp.adapter.PickContactsAdapter;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.Contact;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.other.Constants;

import static kinect.pro.meetingapp.other.Constants.KEY_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_GROUPS;

/**
 * Created on 23.11.2017.
 */

public class PickContactActivity extends BaseMenuActivity implements PickContactsAdapter.OnPickedChangeListener, DatabaseManager.OnDatabaseDataChanged {

    private static final int REQUEST_CREATE_GROUP = 9988;
    @Inject
    SharedPreferences mSharedPreferences;
    @Inject
    DatabaseManager mDatabaseManager;

    @BindView(R.id.contacts_rv)
    RecyclerView mContactsRv;
    @BindView(R.id.group_rv)
    RecyclerView mGroupRv;
    @BindView(R.id.contacts_sv)
    protected SearchView mSearchView;
    @BindView(R.id.picked_tv)
    protected TextView mPickedTv;
    @BindView(R.id.choosed_rl)
    protected View mChoosedLayout;
    @BindView(R.id.new_group_btn)
    protected Button mNewGroupBtn;
    @BindView(R.id.ok_btn)
    protected Button mOkBtn;

    private ArrayList<ContactsModels> mListContactsModels = new ArrayList<>();
    private HashSet<Contact> mListContactsPhone = new HashSet<>();
    private ArrayList<Contact> mFiltered = new ArrayList<>();
    private PickContactsAdapter mAdapter;
    private GroupContactsAdapter mGroupAdapter;
    private ArrayList<UserGroup> mGroupsList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        initUI();
        initContactsList();
        initGroupCreation();
        mDatabaseManager.subscribeToContactsUpdates(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mDatabaseManager.initGroups();
    }

    @Override
    protected int getToolbarResId() {
        return R.layout.header_search;
    }

    private void initUI() {
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                performFilterAndSort(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                performFilterAndSort(newText);
                return false;
            }
        });
        mAdapter = new PickContactsAdapter(mFiltered, mListContactsModels, mGroupsList);
        mContactsRv.setLayoutManager(new LinearLayoutManager(this));
        mContactsRv.setAdapter(mAdapter);
        mAdapter.setOnPickedChangeListener(this);
    }

    @Override
    protected int getCurrentItemId() {
        return R.id.item_calendar;
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_pick_contacts;
    }

    public void initContactsList() {
        mListContactsPhone.clear();
        mFiltered.clear();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds
                .Phone.CONTENT_URI, null, null, null, null);
        try {
            while (phones.moveToNext()) {
                String name = phones.getString(phones
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phone = phones.getString(phones
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String photo = phones.getString
                        (phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                if (photo != null) {
                    mListContactsPhone.add(new Contact(name, phone, Uri.parse(photo)));
                } else {
                    mListContactsPhone.add(new Contact(name, phone));
                }
            }
            phones.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        performFilterAndSort(mSearchView.getQuery().toString());
        if (getIntent().getParcelableArrayListExtra(Constants.KEY_CONTACTS) != null) {
            mAdapter.setPicked(getIntent().getParcelableArrayListExtra(Constants.KEY_CONTACTS));
        }
        checkPickedItems(mAdapter.getPicked());
        mAdapter.notifyDataSetChanged();
    }

    private void initGroupCreation() {
        mGroupAdapter = new GroupContactsAdapter(mAdapter.getPicked(), mListContactsModels);
        mGroupRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mGroupRv.setAdapter(mGroupAdapter);
        mNewGroupBtn.setVisibility(View.VISIBLE);
        mGroupRv.setVisibility(View.INVISIBLE);
        mOkBtn.setText(getString(R.string.ok));
    }

    @OnClick(R.id.new_group_btn)
    void showGroupUI() {
        mGroupRv.setVisibility(View.VISIBLE);
        mNewGroupBtn.setVisibility(View.GONE);
        mOkBtn.setText(getString(R.string.create_group));
    }

    @Override
    public void onBackPressed() {
        if (mNewGroupBtn.getVisibility() == View.GONE) {
            initGroupCreation();
        } else {
            super.onBackPressed();
        }
    }

    private void performFilterAndSort(String text) {
        mFiltered.clear();
        for (Contact contact : mListContactsPhone) {
            if (contact.getName().toLowerCase()
                    .contains(text.toLowerCase())
                    || contact.getPhone().contains(text)) {
                mFiltered.add(contact);
            }
        }
        Collections.sort(mFiltered, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                ContactsModels cm1 = new ContactsModels();
                cm1.setPhone(o1.getPhone());
                ContactsModels cm2 = new ContactsModels();
                cm2.setPhone(o2.getPhone());
                if (mListContactsModels.contains(cm1) && !mListContactsModels.contains(cm2)) {
                    return -1;
                } else if (mListContactsModels.contains(cm2) && !mListContactsModels.contains(cm1)) {
                    return 1;
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.ok_btn)
    void pickContacts() {
        ArrayList<Contact> pickedContacts = mAdapter.getPicked();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_CONTACTS, pickedContacts);
        if (mNewGroupBtn.getVisibility() == View.GONE) {
            Intent newGroupIntent = new Intent(this, CreateGroupActivity.class);
            newGroupIntent.putExtras(bundle);
            startActivityForResult(newGroupIntent, REQUEST_CREATE_GROUP);
        } else {
            Intent result = new Intent();
            result.putExtras(bundle);
            setResult(RESULT_OK, result);
            finish();
        }

    }

    @Override
    public void onPickedChanged(ArrayList<Contact> picked) {
        checkPickedItems(picked);
        mGroupAdapter.notifyDataSetChanged();
    }

    private void checkPickedItems(ArrayList<Contact> picked) {
        if (picked.size() == 0) {
            mChoosedLayout.setVisibility(View.GONE);
        } else {
            mChoosedLayout.setVisibility(View.VISIBLE);
            mPickedTv.setText(String.format(Locale.ENGLISH, getString(R.string.format_choosed), picked.size()));
        }
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        switch (url) {
            case URL_CONTACTS:
                mListContactsModels.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    ContactsModels contactsModels = noteDataSnapshot.getValue(ContactsModels.class);
                    mListContactsModels.add(contactsModels);
                }
                performFilterAndSort(mSearchView.getQuery().toString());
            case URL_GROUPS:
                mGroupsList.clear();
                mGroupsList.addAll(mDatabaseManager.getListUserGroups());
                mGroupAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }
}
