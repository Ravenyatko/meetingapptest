package kinect.pro.meetingapp.activity.user;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.esafirm.imagepicker.features.ImagePicker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.CalendarActivity;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.Profile;
import kinect.pro.meetingapp.other.CircleTransform;
import kinect.pro.meetingapp.other.Constants;

import static kinect.pro.meetingapp.other.Constants.KEY_AVATAR;
import static kinect.pro.meetingapp.other.Constants.URL_PROFILE;

public class ProfileDetailsActivity extends BaseMenuActivity implements DatabaseManager.OnDatabaseDataChanged {

    private static final int REQUEST_CODE_PICKER = 1;

    @BindView(R.id.name_et)
    EditText mNameEt;
    @BindView(R.id.type_rg)
    RadioGroup mTypeRG;
    @BindView(R.id.bussiness_rb)
    RadioButton mBussinessRb;
    @BindView(R.id.private_rb)
    RadioButton mPrivateRb;
    @BindView(R.id.spinner)
    Spinner mSpinner;
    @BindView(R.id.submit_btn)
    Button mSubmitBtn;
    @BindView(R.id.avatar_iv)
    ImageView mAvatarIv;

    @Inject
    SharedPreferences mSharedPrefs;
    @Inject
    DatabaseManager mDatabaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        mDatabaseManager.initProfile();
        if (mSharedPrefs.contains(KEY_AVATAR)) {
            loadImage(mSharedPrefs.getString(KEY_AVATAR, ""));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabaseManager.setDatabaseManagerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDatabaseManager.setDatabaseManagerListener(null);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_profile_details;
    }

    @OnCheckedChanged({R.id.bussiness_rb, R.id.private_rb})
    void setProfileType(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.bussiness_rb:
                    mSpinner.setEnabled(true);
                    break;
                case R.id.private_rb:
                    mSpinner.setEnabled(false);
                    break;
            }
        }
    }

    @OnClick(R.id.submit_btn)
    void submit() {
        if (!mNameEt.getText().toString().isEmpty()) {
            saveProfile();
            startActivity(new Intent(ProfileDetailsActivity.this, CalendarActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else {
            showMessage(getString(R.string.please_fill_name));
        }
    }

    @OnClick(R.id.avatar_iv)
    void setAvatar() {
        ImagePicker.create(this)
                .single()
                .start(REQUEST_CODE_PICKER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<com.esafirm.imagepicker.model.Image> images = (ArrayList<com.esafirm.imagepicker.model.Image>) ImagePicker.getImages(data);
            if (images != null && !images.isEmpty()) {
                mSharedPrefs.edit().putString(KEY_AVATAR, images.get(0).getPath()).apply();
                loadImage(images.get(0).getPath());
            }
        }
    }

    private void loadImage(String path) {
        if (path != null && !path.isEmpty()) {
            Picasso.with(this)
                    .load(new File(path))
                    .transform(new CircleTransform())
                    .into(mAvatarIv);
        } else {
            Picasso.with(ProfileDetailsActivity.this).
                    load(R.mipmap.ic_launcher_round)
                    .transform(new CircleTransform())
                    .into(mAvatarIv);
        }
    }

    private void updateUI() {
        if (mDatabaseManager.getProfile() != null) {
            mNameEt.setText(mDatabaseManager.getProfile().getProfileName());
            if (mDatabaseManager.getProfile().getProfileType().equals(Constants.PROFILE_TYPE_PRIVATE)) {
                mPrivateRb.setChecked(true);
            } else {
                mBussinessRb.setChecked(true);
            }
            mSpinner.setSelection(mDatabaseManager.getProfile().getProfileBusinessType());
        } else {
            lockNavigation();
        }
    }

    @Override
    protected void lockNavigation() {
        super.lockNavigation();
        mPrivateRb.setChecked(true); // check private by default
    }

    @Override
    protected int getCurrentItemId() {
        return R.id.item_profile;
    }

    private void saveProfile() {
        mDatabaseManager.saveProfile(new Profile(mNameEt.getText().toString(),
                mPrivateRb.isChecked() ? Constants.PROFILE_TYPE_PRIVATE : Constants.PROFILE_TYPE_BUSINESS,
                mSpinner.getSelectedItemPosition()));
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        if (url.equals(URL_PROFILE)) {
            updateUI();
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }
}
