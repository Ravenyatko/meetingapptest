package kinect.pro.meetingapp.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import com.ebanx.swipebtn.SwipeButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.other.Constants;

public class PopupActivity extends AppCompatActivity {

    @BindView(R.id.meeting_tv)
    TextView mMeetingTv;
    @BindView(R.id.swipe_btn)
    SwipeButton mEnableBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        setContentView(R.layout.popup_layout);
        ButterKnife.bind(this);

        String stringExtra = getIntent().getStringExtra(Constants.POPUP_EXTRA);
        mMeetingTv.setText(stringExtra);

        mEnableBtn.setOnStateChangeListener(active -> finish());
    }
}