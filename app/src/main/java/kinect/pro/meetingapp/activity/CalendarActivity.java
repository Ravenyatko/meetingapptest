package kinect.pro.meetingapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.activity.meeting.CreateMeetingActivity;
import kinect.pro.meetingapp.activity.meeting.InfoMeetingActivity;
import kinect.pro.meetingapp.adapter.MeetingsAdapter;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.Participant;
import kinect.pro.meetingapp.other.Utils;

import static kinect.pro.meetingapp.other.Constants.DEFAULT;
import static kinect.pro.meetingapp.other.Constants.KEY_INFO_EVENT;
import static kinect.pro.meetingapp.other.Constants.KEY_PHONE;
import static kinect.pro.meetingapp.other.Constants.TYPE_CHRONOLOGY_VIEW;
import static kinect.pro.meetingapp.other.Constants.TYPE_DAY_VIEW;
import static kinect.pro.meetingapp.other.Constants.TYPE_MONTH_VIEW;
import static kinect.pro.meetingapp.other.Constants.URL_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_MEETING;

public class CalendarActivity extends BaseMenuActivity implements MonthLoader.MonthChangeListener,
        WeekView.EventClickListener, DatabaseManager.OnDatabaseDataChanged, MeetingsAdapter.OnMeetingClickListener {

    private static final String TAG = "CalendarActivity ==>> ";
    private static final int REQUEST_CONTACTS_PERMISSION = 1001;

    @Inject
    SharedPreferences mSharedPreferences;
    @Inject
    DatabaseManager mDatabaseManager;

    @BindView(R.id.week_view)
    WeekView mWeekView;
    @BindView(R.id.items_rv)
    RecyclerView mMeetingsRv;

    private MeetingsAdapter mAdapter;
    private int currentCalendarType = TYPE_DAY_VIEW;
    private ArrayList<MeetingModel> mMeetingModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        initChronology();
    }

    private void initChronology() {
        mAdapter = new MeetingsAdapter(mMeetingModels, mSharedPreferences.getString(KEY_PHONE, DEFAULT));
        mMeetingsRv.setLayoutManager(new LinearLayoutManager(this));
        mMeetingsRv.setAdapter(mAdapter);
    }

    @Override
    protected int getMenuResource() {
        return R.menu.activity_menu_calendar;
    }

    @Override
    protected int getCurrentItemId() {
        return R.id.item_chronological;
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_calendar;
    }

    @OnClick(R.id.create_meeting_fab)
    void createMeeting() {
        if (isPermissionGranted()) {
            startActivity(new Intent(CalendarActivity.this, CreateMeetingActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else {
            requestPermission();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_chronological:
                currentCalendarType = TYPE_CHRONOLOGY_VIEW;
                break;
            case R.id.item_day:
                currentCalendarType = TYPE_DAY_VIEW;
                break;
            case R.id.item_month:
                currentCalendarType = TYPE_MONTH_VIEW;
                break;
            default:
                return super.onNavigationItemSelected(item);
        }
        setCurrentCalendarView();
        closeDrawer();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.setOnMeetingClickListener(this);
        mDatabaseManager.setDatabaseManagerListener(this);
        updateView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdapter.setOnMeetingClickListener(null);
        mDatabaseManager.setDatabaseManagerListener(null);
    }

    private void updateView() {
        mDatabaseManager.initMeeting();
        initWeekView();
    }

    private void initWeekView() {
        mWeekView.setMonthChangeListener(this);
        mWeekView.setOnEventClickListener(this);
        setCurrentCalendarView();
    }

    private void setCurrentCalendarView() {
        switch (currentCalendarType) {
            case TYPE_CHRONOLOGY_VIEW:
                mWeekView.setVisibility(View.GONE);
                mMeetingsRv.setVisibility(View.VISIBLE);
                setCurrentItem(R.id.item_chronological);
                break;
            case TYPE_DAY_VIEW:
                mWeekView.setVisibility(View.VISIBLE);
                mMeetingsRv.setVisibility(View.GONE);
                mWeekView.setNumberOfVisibleDays(1);
                mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                mWeekView.goToToday();
                mWeekView.notifyDatasetChanged();
                setCurrentItem(R.id.item_day);
                break;
            case TYPE_MONTH_VIEW:
                mWeekView.setVisibility(View.VISIBLE);
                mMeetingsRv.setVisibility(View.GONE);
                mWeekView.setNumberOfVisibleDays(5);
                mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                mWeekView.goToToday();
                mWeekView.notifyDatasetChanged();
                setCurrentItem(R.id.item_month);
                break;
        }
    }

    //adding a meeting to the calendar
    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        ArrayList<WeekViewEvent> eventsMonth = new ArrayList<>();
        mMeetingModels.clear();
        mMeetingModels.addAll(mDatabaseManager.getMeetingModels());
        mAdapter.notifyDataSetChanged();
        List<WeekViewEvent> mEvents = new ArrayList<>();
        for (int i = 0; i < mMeetingModels.size(); i++) {
            StringBuilder participants = new StringBuilder();
            Calendar startTime = Calendar.getInstance();
            startTime.setTimeInMillis(mMeetingModels.get(i).getDate());
            Calendar endTime = Calendar.getInstance();
            endTime.setTimeInMillis(mMeetingModels.get(i).getDuration());

            participants.append("\n");
            for (int j = 0; j < mMeetingModels.get(i).getParticipants().size(); j++) {
                Participant participant = mMeetingModels.get(i).getParticipants().get(j);
                if(participant == null){
                    continue;
                }
                String label = (participant.getName() == null || participant.getName().isEmpty()) ? participant.getPhone() : participant.getName();
                if (j > 0 && j < mMeetingModels.get(i).getParticipants().size()) {
                    participants.append(", ");
                }
                participants.append(label);
            }

            WeekViewEvent event = new WeekViewEvent(i,
                    mMeetingModels.get(i).getTopic(),
                    participants.toString(),
                    startTime,
                    endTime);
            event.setColor(getResources().getColor(Utils.getMeetingColor(mMeetingModels.get(i))));
            mEvents.add(event);
        }

        for (int i = 0; i < mEvents.size(); i++) {
            if (mEvents.get(i).getStartTime().get(Calendar.MONTH) == newMonth - 1
                    && mEvents.get(i).getStartTime().get(Calendar.YEAR) == newYear) {
                eventsMonth.add(mEvents.get(i));
            }
        }
        return eventsMonth;
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        showDetails(mMeetingModels.get((int) event.getId()));
    }

    @Override
    public void onMeetingClick(MeetingModel meeting) {
        showDetails(meeting);
    }

    private void showDetails(MeetingModel item) {
        Gson gson = new Gson();
        String json = gson.toJson(item);
        mSharedPreferences.edit().putString(KEY_INFO_EVENT, json).apply();
        startActivity(new Intent(CalendarActivity.this, InfoMeetingActivity.class));
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        switch (url) {
            case URL_CONTACTS:
            case URL_MEETING:
                mMeetingModels.clear();
                mMeetingModels.addAll(mDatabaseManager.getMeetingModels());
                mWeekView.notifyDatasetChanged();
                mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {
        //ignore
    }


    private boolean isPermissionGranted() {
        int permissionCheck = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_CONTACTS);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createMeeting();
            } else {
                showMessage(R.string.permission_denied);
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_CONTACTS_PERMISSION);
    }

}
