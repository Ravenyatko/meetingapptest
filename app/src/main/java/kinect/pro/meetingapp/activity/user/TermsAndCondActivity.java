package kinect.pro.meetingapp.activity.user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kinect.pro.meetingapp.R;

public class TermsAndCondActivity extends AppCompatActivity {

    public static final String TERMS_URL = "file:///android_asset/index.html";

    @BindView(R.id.terms_webview)
    WebView mWebViewTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_cond);
        ButterKnife.bind(this);

        mWebViewTerms.loadUrl(TERMS_URL);
    }

    @OnClick(R.id.image_back)
    void back(){
        onBackPressed();
    }
}
