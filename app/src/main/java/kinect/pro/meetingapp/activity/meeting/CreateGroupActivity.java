package kinect.pro.meetingapp.activity.meeting;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.base.BaseActivity;
import kinect.pro.meetingapp.adapter.GroupContactsAdapter;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.Contact;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.model.WeekDayTimeModel;
import kinect.pro.meetingapp.view.DayTimePickerView;

import static kinect.pro.meetingapp.other.Constants.KEY_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_CONTACTS;

/**
 * Created on 30.11.2017.
 */

public class CreateGroupActivity extends BaseActivity implements DatabaseManager.OnDatabaseDataChanged {


    @Inject
    DatabaseManager mDatabaseManager;
    @BindView(R.id.group_rv)
    RecyclerView mGroupRv;
    @BindView(R.id.users_count_tv)
    TextView mCountTv;
    @BindView(R.id.group_name_til)
    TextInputLayout mGroupNameTil;
    @BindView(R.id.group_name_et)
    EditText mGroupNameEt;
    @BindView(R.id.scroll_view)
    ScrollView mScrollView;
    @BindView(R.id.times_ll)
    LinearLayout mTimesLl;

    private GroupContactsAdapter mGroupAdapter;
    private ArrayList<Contact> mPicked = new ArrayList<>();
    private ArrayList<ContactsModels> mListContactsModels = new ArrayList<>();
    private ArrayList<Contact> mListContactsPhone = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_group);
        ((App) getApplication()).getAppComponent().inject(this);
        ButterKnife.bind(this);
        mDatabaseManager.subscribeToContactsUpdates(this);
        mDatabaseManager.initGroups();
        initContactsList();
        getDevicecontacts();
    }

    private void initContactsList() {
        mPicked = getIntent().getParcelableArrayListExtra(KEY_CONTACTS);
        if (mPicked == null) {
            finish();
            return;
        }
        mGroupAdapter = new GroupContactsAdapter(mPicked, mListContactsModels);
        mGroupRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mGroupRv.setAdapter(mGroupAdapter);
        mCountTv.setText(String.format(getString(R.string.users_count_format), mPicked.size()));
    }

    @OnClick(R.id.back_btn)
    void backClick() {
        onBackPressed();
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        if (url.equals(URL_CONTACTS)) {
            mListContactsModels.clear();
            for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                ContactsModels contactsModels = noteDataSnapshot.getValue(ContactsModels.class);
                mListContactsModels.add(contactsModels);
            }
            mGroupAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }

    @OnClick(R.id.submit_btn)
    void submit() {
        ArrayList<DayTimePickerView> currentViews = getDayViewsList();
        ArrayList<WeekDayTimeModel> dataItems = new ArrayList<>();

        boolean isDataValid = true;
        String groupName = mGroupNameEt.getText().toString().trim();
        if (groupName.isEmpty()) {
            mGroupNameTil.setError(getString(R.string.fill_group_name_error));
            isDataValid = false;
        }
        for (UserGroup userGroup:mDatabaseManager.getListUserGroups()){
            if(userGroup.getName().equalsIgnoreCase(groupName)){
                mGroupNameTil.setError(getString(R.string.this_name_is_used));
                isDataValid = false;
            }
        }

        for (DayTimePickerView view : currentViews) {
            if (!view.validate()) {
                isDataValid = false;
            }
            if (dataItems.contains(view.getData())) {
                isDataValid = false;
                showMessage(R.string.different_days_error);
            }
            dataItems.add(view.getData());
        }
        for (Contact contact : mListContactsPhone) {
            if (contact.getName().equalsIgnoreCase(groupName)) {
                showMessage(R.string.group_name_error);
                isDataValid = false;
            }
        }
        if (isDataValid) {
            UserGroup userGroup = new UserGroup(groupName, dataItems, mPicked);
            mDatabaseManager.createNewGroup(groupName, userGroup);
            setResult(RESULT_OK);
            finish();
        }

    }

    @OnClick(R.id.add_time_btn)
    void addNewTimeView() {
        DayTimePickerView dayTimePickerView = new DayTimePickerView(this);
        ArrayList<DayTimePickerView> currentViews = getDayViewsList();
        if (currentViews.size() > 0 && currentViews.get(currentViews.size() - 1).getData().getDayOfWeek() < 7) {
            dayTimePickerView.setCurrentDay(currentViews.get(currentViews.size() - 1).getData().getDayOfWeek() + 1);
        }
        mTimesLl.addView(dayTimePickerView);
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    private ArrayList<DayTimePickerView> getDayViewsList() {
        ArrayList<DayTimePickerView> views = new ArrayList<>();
        for (int i = 0; i < mTimesLl.getChildCount(); i++) {
            if (mTimesLl.getChildAt(i) instanceof DayTimePickerView) {
                views.add((DayTimePickerView) mTimesLl.getChildAt(i));
            }
        }
        return views;
    }

    public void getDevicecontacts() {
        HashSet<Contact> contactsSet = new HashSet<>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds
                .Phone.CONTENT_URI, null, null, null, null);
        try {
            while (phones.moveToNext()) {
                contactsSet.add(new Contact(phones.getString(phones
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)),
                        phones.getString(phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))));
            }
            phones.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mListContactsPhone.clear();
        mListContactsPhone.addAll(contactsSet);
    }

}
