package kinect.pro.meetingapp.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.CalendarActivity;
import kinect.pro.meetingapp.activity.base.BaseActivity;
import kinect.pro.meetingapp.auth.AuthManager;
import kinect.pro.meetingapp.firebase.DatabaseManager;

import static kinect.pro.meetingapp.other.Constants.URL_PROFILE;


public class LoginActivity extends BaseActivity implements AuthManager.OnVerificationListener, AuthManager.AuthStateListener, DatabaseManager.OnDatabaseDataChanged {

    private static final String TAG = "LoginActivity ==>> ";

    @Inject
    AuthManager mAuthManager;

    @BindView(R.id.number_et)
    EditText mCodeNumberEt;
    @BindView(R.id.ok_btn)
    Button mOkBtn;

    private String mVerificationId;
    @Inject
    DatabaseManager mDatabaseManager;
    private boolean isLoggedInAlready = false;

    @Override
    protected void onStart() {
        super.onStart();
        mAuthManager.setVerificationListener(this);
        mAuthManager.setAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuthManager.setVerificationListener(null);
        mAuthManager.setAuthStateListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ((App) getApplication()).getAppComponent().inject(this);
        setButtonTextToRegistration();
    }

    @OnClick(R.id.ok_btn)
    public void onClick(View view) {
        if (mOkBtn.getText().toString().equals(getResources().getString(R.string.registration))) {
            alertDialog();
        } else {
            try {
                String code = mCodeNumberEt.getText().toString();
                if (TextUtils.isEmpty(code))
                    return;
                mAuthManager.signInWithCredential(PhoneAuthProvider.getCredential(mVerificationId, code));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void alertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        alertDialogBuilder
                .setTitle(R.string.please_confirm_phone)
                .setMessage(mCodeNumberEt.getText().toString())
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    try {
                        handleRequestCode();
                        setButtonTextToValidation();
                    } catch (FirebaseTooManyRequestsException e) {
                        e.getMessage();
                    }
                })
                .setNegativeButton(R.string.edit, (dialog, id) -> {
                    dialog.cancel();
                }).show();
    }

    @OnClick(R.id.terms_and_cond)
    public void onClickTermsAndCondition(View view) {
        startActivity(new Intent(LoginActivity.this, TermsAndCondActivity.class));
    }

    private void setButtonTextToValidation() {
        mCodeNumberEt.setText("");
        mOkBtn.setText(getResources().getText(R.string.submit));
        mCodeNumberEt.setHint(R.string.validation_Code);
    }

    private void setButtonTextToRegistration() {
        mCodeNumberEt.setText("");
        mOkBtn.setText(getResources().getText(R.string.registration));
        mCodeNumberEt.setHint(R.string.enter_phone_number);
    }

    private void handleRequestCode() throws FirebaseTooManyRequestsException {
        String phoneNumber = mCodeNumberEt.getText().toString();
        if (phoneNumber.isEmpty()) {
            mCodeNumberEt.setError(getString(R.string.number_not_entered));
            return;
        }

        if (!phoneNumber.startsWith("+")) {
            mCodeNumberEt.setError(getString(R.string.number_worng_format));
            return;
        }
        mAuthManager.requestCode(this, phoneNumber);
    }

    @Override
    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
        mAuthManager.signInWithCredential(phoneAuthCredential);
        showProgress(true);
    }

    @Override
    public void onVerificationFailed(FirebaseException e) {
        mCodeNumberEt.setError(getString(R.string.verification_failed));
        showMessage(e.getMessage());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabaseManager.setDatabaseManagerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDatabaseManager.setDatabaseManagerListener(null);
    }

    @Override
    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        mVerificationId = verificationId;
    }

    @Override
    public void onCodeAutoRetrievalTimeOut(String verificationId) {
        showMessage(R.string.verification_failed_timeout);
        setButtonTextToRegistration();
    }

    @Override
    public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
        if (firebaseAuth.getCurrentUser() != null) {
            mDatabaseManager.initProfile();
        }
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        if (url.equals(URL_PROFILE)) {
            showProgress(false);
            if (mDatabaseManager.getProfile() != null) {
                startActivity(new Intent(LoginActivity.this, CalendarActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK));
            } else {
                startActivity(new Intent(LoginActivity.this, ProfileDetailsActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            isLoggedInAlready = true;
            finish();
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }


    @Override
    public void onSignIn(boolean isSuccessful) {
        if (isLoggedInAlready) {
            return; //handle situation when code was already processed
        }
        if (isSuccessful) {
            mDatabaseManager.initProfile();
        } else {
            showProgress(false);
            showMessage(R.string.failed_sign_in);
            setButtonTextToRegistration();
        }
    }

    @Override
    public void onBackPressed() {
        if (mOkBtn.getText().toString().equals(getResources().getString(R.string.submit))) {
            setButtonTextToRegistration();
        } else {
            super.onBackPressed();
        }
    }
}

