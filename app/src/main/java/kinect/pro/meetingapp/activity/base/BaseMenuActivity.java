package kinect.pro.meetingapp.activity.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.CalendarActivity;
import kinect.pro.meetingapp.activity.user.ProfileDetailsActivity;

import static android.support.v4.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED;

/**
 * Created on 23.11.2017.
 */

public abstract class BaseMenuActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.nav_bar)
    FrameLayout mNavigationBar;


    protected FrameLayout mContentLayout;
    private Runnable mPendingRunnable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mContentLayout = findViewById(R.id.content); //need this to inflate before butterknife initalized
        mNavigationBar = findViewById(R.id.nav_bar); //need this to inflate before butterknife initalized
        getLayoutInflater().inflate(getActivityLayout(), mContentLayout, true);
        inflateNavigation();
        ButterKnife.bind(this);
        setupDrawer();
    }

    private void inflateNavigation() {
        getLayoutInflater().inflate(getToolbarResId(), mNavigationBar, true);
    }

    protected int getToolbarResId() {
        return R.layout.header;
    }

    @Override
    protected void onStart() {
        super.onStart();
        setCurrentItem(getCurrentItemId());
    }

    protected abstract int getCurrentItemId();

    protected abstract int getActivityLayout();

    private void setupDrawer() {
        mDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
//                setupProfile(drawerHeader);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if (mPendingRunnable != null) {
                    runOnUiThread(mPendingRunnable);
                    mPendingRunnable = null;
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        mNavView.getMenu().clear();
        mNavView.inflateMenu(getMenuResource());
        mNavView.setNavigationItemSelectedListener(this);
    }

    protected int getMenuResource() {
        return R.menu.activity_main_drawer;
    }

    private Runnable createPendingIntent(final Class activityClass) {
        return new Runnable() {
            @Override
            public void run() {
                Intent callIntent = new Intent(BaseMenuActivity.this, activityClass);
                startActivity(callIntent);
            }
        };
    }

    protected void lockNavigation() {
        mNavigationBar.setVisibility(View.INVISIBLE);
        mDrawer.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED);
    }

    @Optional
    @OnClick(R.id.home_iv)
    void onHomePressed() {
        if (!BaseMenuActivity.this.getClass().getSimpleName().equals(CalendarActivity.class.getSimpleName())) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @OnClick(R.id.drawer_iv)
    void toggleDrawer() {
        mDrawer.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Class newActivityClass = null;
        switch (item.getItemId()) {
            case R.id.item_profile:
                newActivityClass = ProfileDetailsActivity.class;
                break;
            case R.id.item_calendar:
                newActivityClass = CalendarActivity.class;
                break;
        }
        if (newActivityClass != null) {
            if (!BaseMenuActivity.this.getClass().getName().equals(newActivityClass.getName())) {
                mPendingRunnable = createPendingIntent(newActivityClass);
            }
        }
        closeDrawer();
        return true;
    }

    protected void setCurrentItem(int id) {
        mNavView.setCheckedItem(id);
    }

    protected void closeDrawer() {
        mDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


}
