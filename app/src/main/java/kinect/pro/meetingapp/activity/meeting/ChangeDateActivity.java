package kinect.pro.meetingapp.activity.meeting;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.fastaccess.datetimepicker.callback.TimePickerCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.Participant;
import kinect.pro.meetingapp.other.Constants;
import kinect.pro.meetingapp.rest.RestController;
import kinect.pro.meetingapp.utils.DateTimeGapFinder;
import kinect.pro.meetingapp.utils.TimeUtil;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.UserGroup;

import static java.util.Locale.ENGLISH;
import static kinect.pro.meetingapp.other.Constants.DATE_FORMAT;
import static kinect.pro.meetingapp.other.Constants.DEFAULT;
import static kinect.pro.meetingapp.other.Constants.KEY_INFO_EVENT;
import static kinect.pro.meetingapp.other.Constants.KEY_PHONE;
import static kinect.pro.meetingapp.other.Constants.URL_GROUPS;
import static kinect.pro.meetingapp.other.Constants.URL_MEETING;

/**
 * Created on 01.12.2017.
 */

public class ChangeDateActivity extends BaseMenuActivity implements DatabaseManager.OnDatabaseDataChanged, DatePickerCallback, TimePickerCallback {

    private final static int SUGGESTIONS_COUNT = 2;

    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DatabaseManager mDatabaseManager;
    @Inject
    RestController mRestController;
    @Inject
    SharedPreferences mSharedPreferences;

    @BindView(R.id.datetime_et)
    EditText mDateTimeEt;
    @BindView(R.id.duration_et)
    EditText mDurationEt;
    @BindView(R.id.manual_ll)
    View mManualSetLayout;
    @BindView(R.id.auto_time_label)
    View mAutoTimeLabel;

    private DatabaseReference mDatabaseReference;
    private DatabaseReference mGroupsReference;
    private FirebaseDatabase mDatabase;
    private MeetingModel mCurrentMeeting;

    @BindView(R.id.time_rg)
    RadioGroup mTimeRg;

    private ArrayList<PurposedDate> mPurposedDates = new ArrayList<>();
    private ArrayList<MeetingModel> mAllMeetingsList = new ArrayList<>();
    private ArrayList<UserGroup> mCurrentMeetingGroups = new ArrayList<>();

    private int mFirstAlwaysDate = 0;

    @Override
    protected int getCurrentItemId() {
        return R.id.item_calendar;
    }


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_change_date;
    }

    @OnClick(R.id.cancelled_btn)
    void cancel() {
        finish();
    }

    @OnClick(R.id.submit_btn)
    void submit() {

        if (mTimeRg.getCheckedRadioButtonId() == -1 && mManualSetLayout.getVisibility() == View.GONE) {
            showMessage(R.string.please_choose_time);
        } else if (mManualSetLayout.getVisibility() == View.VISIBLE) {
            if (mCurrentMeeting.getDate() > mCurrentMeeting.getDuration()) {
                showMessage(R.string.error_start_time_before_end);
                return;
            }

            if (mCurrentMeeting.getDuration() - mCurrentMeeting.getDate() < 5 * 60 * 1000) { //5 mins
                showMessage(R.string.error_min_time);
                return;
            }
            if (!TimeUtil.isGroupsMatched(mCurrentMeeting.getDate(), mCurrentMeeting.getDuration(), mCurrentMeetingGroups)) {
                showMessage(R.string.group_time_error);
                return;
            }
            if (!TimeUtil.isMeetingsMatched(mCurrentMeeting, mAllMeetingsList)) {
                showMessage(R.string.time_conflict);
                return;
            }
            saveAndNotify();
        } else {
            int position = mTimeRg.indexOfChild(mTimeRg.findViewById(mTimeRg.getCheckedRadioButtonId()));
            mCurrentMeeting.setDate(mPurposedDates.get(position).getStart());
            mCurrentMeeting.setDuration(mPurposedDates.get(position).getEnd());
            saveAndNotify();
        }
    }

    private void saveAndNotify() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH);
        for (Participant participant : mCurrentMeeting.getParticipants()) {
            if (!participant.getPhone().equals(mSharedPreferences.getString(KEY_PHONE, DEFAULT))) {
                participant.setStatus(Constants.STATUS_PENDING);
                mRestController.notifyTimeChanged(this, participant.getPhone(),
                        mCurrentMeeting.getTopic().trim(),
                        simpleDateFormat.format(new Date(mCurrentMeeting.getDate())),
                        simpleDateFormat.format(new Date(mCurrentMeeting.getDuration())));
            }
        }
        Gson gson = new Gson();
        String json = gson.toJson(mCurrentMeeting);
        mSharedPreferences.edit().putString(KEY_INFO_EVENT, json).apply();
        mDatabaseManager.addOrUpdateMeeting(mCurrentMeeting, mCurrentMeeting.getTopic().trim());
        finish();
    }

    @OnClick(R.id.set_btn)
    void setTimeManually() {
        mManualSetLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabaseManager.setDatabaseManagerListener(this);
        mDatabaseManager.initMeeting();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDatabaseManager.setDatabaseManagerListener(null);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        mDatabase = FirebaseDatabase.getInstance();
        Gson gson = new Gson();
        MeetingModel item = gson.fromJson(sharedPreferences.getString(KEY_INFO_EVENT, null), MeetingModel.class);
        initFirebase(item);
    }

    private void fillMeetingGroups() {
        if (mCurrentMeeting.getGroups() == null) {
            return;
        }
        mGroupsReference = mDatabase.getReference(URL_GROUPS + mCurrentMeeting.getAuthor() + "/");
        mGroupsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mCurrentMeetingGroups.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    UserGroup group = noteDataSnapshot.getValue(UserGroup.class);
                    if (group != null && mCurrentMeeting.getGroups().contains(group.getName())) {
                        mCurrentMeetingGroups.add(group);
                    }
                }
                showNextDates();
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    private void initUI() {
        Drawable calIcon = AppCompatResources.getDrawable(this, R.drawable.ic_date_range_black_24dp);
        mDateTimeEt.setCompoundDrawablesWithIntrinsicBounds(null, null, calIcon, null);
        mDurationEt.setCompoundDrawablesWithIntrinsicBounds(null, null, calIcon, null);
        mDateTimeEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(mCurrentMeeting.getDate()));
        mDurationEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(mCurrentMeeting.getDuration()));
    }

    private void initFirebase(MeetingModel meetingModel) {
        mDatabaseReference = mDatabase.getReference(URL_MEETING);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    MeetingModel mModels = noteDataSnapshot.getValue(MeetingModel.class);
                    if (mModels != null) {
                        if (mModels.getTopic().equals(meetingModel.getTopic()) &&
                                mModels.getAuthor().equals(meetingModel.getAuthor())
                                && mModels.getDate() == meetingModel.getDate()) {
                            if (mModels.getParticipants().get(0).getStatus() != null) {
                                mCurrentMeeting = mModels;
                                initUI();
                                fillMeetingGroups();
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        switch (url) {
            case URL_MEETING:
                mAllMeetingsList.clear();
                mAllMeetingsList.addAll(mDatabaseManager.getMeetingModels());
                if (mCurrentMeeting.getGroups() == null) {
                    showNextDates();
                } else {
                    fillMeetingGroups();
                }
                break;
        }
    }

    @OnFocusChange(R.id.datetime_et)
    public void onClickDataTime(View view, boolean isFocused) {
        if (isFocused) {
            mFirstAlwaysDate = 1;
            showDatePickerFragment(System.currentTimeMillis());
        }
    }

    @OnClick(R.id.datetime_et)
    public void onClickDataTime(View view) {
        mFirstAlwaysDate = 1;
        showDatePickerFragment(System.currentTimeMillis());
    }

    @OnFocusChange(R.id.duration_et)
    public void onClickDuration(View view, boolean isFocused) {
        if (isFocused) {
            mFirstAlwaysDate = 2;
            showDatePickerFragment(mCurrentMeeting.getDate());
        }
    }

    @OnClick(R.id.duration_et)
    public void onClickDuration(View view) {
        mFirstAlwaysDate = 2;
        showDatePickerFragment(mCurrentMeeting.getDate());
    }

    private void showDatePickerFragment(long minDate) {
        if (minDate == 0)
            minDate = System.currentTimeMillis();
        DatePickerFragmentDialog.newInstance(
                DateTimeBuilder.newInstance()
                        .withTime(true)
                        .with24Hours(true)
                        .withSelectedDate(minDate)
                        .withMinDate(System.currentTimeMillis())
                        .withTheme(R.style.PickersTheme))
                .show(getSupportFragmentManager(), "StartDatePickerFragmentDialog");
    }

    private void showNextDates() {
        mPurposedDates.clear();
        long duration = mCurrentMeeting.getDuration() - mCurrentMeeting.getDate();
        Collections.sort(mAllMeetingsList, new Comparator<MeetingModel>() {
            @Override
            public int compare(MeetingModel o1, MeetingModel o2) {
                Long start1 = o1.getDate();
                Long start2 = o2.getDate();
                return start1.compareTo(start2);
            }
        });

        List<Interval> filteredIntervals = new ArrayList<>();

        long minStartTime = mAllMeetingsList.get(0).getDate();
        long maxEndTime = mAllMeetingsList.get(0).getDuration();

        for (int i = 0; i < mAllMeetingsList.size(); i++) {
            MeetingModel item = mAllMeetingsList.get(i);

            minStartTime = Math.min(minStartTime, item.getDate());
            maxEndTime = Math.max(maxEndTime, item.getDuration());

            if (!Collections.disjoint(item.getParticipants(), mCurrentMeeting.getParticipants())
                    && item.getDuration() >= mCurrentMeeting.getDate()) {
                // we need to check only meetings with similar users and with end date >= our start date
                filteredIntervals.add(new Interval(item.getDate(), item.getDuration()));
            }
        }

        maxEndTime = Math.max(mCurrentMeeting.getDuration(), maxEndTime);

        List<Interval> gaps = DateTimeGapFinder.findGaps(filteredIntervals, new Interval(mCurrentMeeting.getDate(), new DateTime(maxEndTime + duration * 2).plusDays(40).getMillis()));
        for (Interval gap : gaps) {
            long gapDuration = gap.toDurationMillis();
            if (gapDuration >= duration) {
                int possibleCount = (int) (gapDuration / duration);
                for (int i = 0; i < possibleCount; i++) {
                    long possibleStart = gap.getStartMillis() + duration * i;
                    long possibleEnd = gap.getStartMillis() + duration * i + duration;
                    if (mPurposedDates.size() < SUGGESTIONS_COUNT && TimeUtil.isGroupsMatched(possibleStart, possibleEnd, mCurrentMeetingGroups)) {
                        mPurposedDates.add(new PurposedDate(possibleStart, possibleEnd));
                    }
                }
            }
        }

        showSuggestions();
    }

    private void showSuggestions() {
        mTimeRg.removeAllViews();
        if (mPurposedDates.size() > 0) {
            mAutoTimeLabel.setVisibility(View.VISIBLE);
            for (PurposedDate date : mPurposedDates) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setText(date.toString());
                mTimeRg.addView(radioButton);
            }
        } else {
            mAutoTimeLabel.setVisibility(View.GONE);
            setTimeManually();
        }
    }

    private boolean mathesGroupsTime(long startTime, long endTime) {
        return TimeUtil.isGroupsMatched(startTime, endTime, mCurrentMeetingGroups);
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }

    @Override
    public void onDateSet(long date) {

    }

    @Override
    public void onTimeSet(long timeOnly, long dateWithTime) {
        long startTime = mCurrentMeeting.getDate();
        long endTime = mCurrentMeeting.getDuration();
        if (mFirstAlwaysDate == 1) {
            if (dateWithTime < System.currentTimeMillis()) {
                Toast.makeText(this, getString(R.string.error_time_passed), Toast.LENGTH_SHORT).show();
                return;
            }
            startTime = new DateTime(dateWithTime).withSecondOfMinute(0).withMillisOfSecond(0).getMillis();
            mDateTimeEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(dateWithTime));
            mFirstAlwaysDate = 0;
        }
        if (mFirstAlwaysDate == 2) {
            if (startTime < dateWithTime) {
                endTime = new DateTime(dateWithTime).withSecondOfMinute(0).withMillisOfSecond(0).getMillis();
                if (dateWithTime < System.currentTimeMillis() || dateWithTime < startTime) {
                    Toast.makeText(this, getString(R.string.error_time_passed), Toast.LENGTH_SHORT).show();
                    return;
                }
                mDurationEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(dateWithTime));
                mFirstAlwaysDate = 0;
            } else {
                Toast.makeText(this, getString(R.string.error_start_time_before_end), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        mCurrentMeeting.setDate(startTime);
        mCurrentMeeting.setDuration(endTime);
        initUI();
    }

    class PurposedDate {
        long start;
        long end;

        public PurposedDate(long start, long end) {
            this.start = start;
            this.end = end;
        }

        public long getStart() {
            return start;
        }

        public void setStart(long start) {
            this.start = start;
        }

        public long getEnd() {
            return end;
        }

        public void setEnd(long end) {
            this.end = end;
        }

        @Override
        public String toString() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
            return String.format("%s - %s",
                    simpleDateFormat.format(new Date(start)),
                    simpleDateFormat.format(new Date(end))
            );
        }
    }
}
