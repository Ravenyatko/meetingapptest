package kinect.pro.meetingapp.activity.meeting;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.content.res.AppCompatResources;
import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.fastaccess.datetimepicker.callback.TimePickerCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.utils.TimeUtil;
import kinect.pro.meetingapp.activity.CalendarActivity;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.adapter.ContactsAdapter;
import kinect.pro.meetingapp.firebase.DatabaseManager;
import kinect.pro.meetingapp.model.AutocompleteItemContact;
import kinect.pro.meetingapp.model.Contact;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.Participant;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.other.Constants;
import kinect.pro.meetingapp.other.Utils;
import kinect.pro.meetingapp.rest.RestController;

import static java.util.Locale.ENGLISH;
import static kinect.pro.meetingapp.other.Constants.DATE_FORMAT;
import static kinect.pro.meetingapp.other.Constants.DEFAULT;
import static kinect.pro.meetingapp.other.Constants.KEY_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.KEY_PHONE;
import static kinect.pro.meetingapp.other.Constants.PLACE_PICKER_REQUEST;
import static kinect.pro.meetingapp.other.Constants.URL_CONTACTS;
import static kinect.pro.meetingapp.other.Constants.URL_GROUPS;
import static kinect.pro.meetingapp.popup.PopupAlarmManager.scheduleNotification;


public class CreateMeetingActivity extends BaseMenuActivity
        implements DatePickerCallback, TimePickerCallback,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener,
        DatabaseManager.OnDatabaseDataChanged {

    private final static String TAG = "CreateMeetingActivity";
    private static GoogleApiClient mGoogleApiClient;

    @Inject
    SharedPreferences mSharedPreferences;
    @Inject
    RestController mRestController;
    @Inject
    DatabaseManager mDatabaseManager;

    @BindView(R.id.meeting_topic_et)
    EditText mMeetingTopicEt;
    @BindView(R.id.contacts_et)
    MultiAutoCompleteTextView mContactsEt;
    @BindView(R.id.datetime_et)
    EditText mDateTimeEt;
    @BindView(R.id.duration_et)
    EditText mDurationEt;
    @BindView(R.id.location_et)
    EditText mLocationEt;
    @BindView(R.id.reminder_et)
    EditText mRemiderEt;

    private ArrayList<ContactsModels> mListContactsModels;
    private ArrayList<Participant> mListParticipant;
    private ArrayList<Contact> mListContactsPhone;
    private ArrayList<AutocompleteItemContact> mAutocompleteList;
    private ArrayList<UserGroup> mGroupsList;
    private ContactsAdapter mAdapterContacts;

    private ArrayList<Contact> pickedContacts = new ArrayList<>();
    private ArrayList<UserGroup> pickedGroups = new ArrayList<>();

    private int mFirstAlwaysDate = 0;
    private float mLatitude = 0L;
    private float mLongitude = 0L;
    private long mStartTime = 0;
    private long mStopTime = 0;
    private Handler mHandler = new Handler();

    private static final int CONTACT_PICKER_RESULT = 1001;

    @Override
    protected void onStart() {
        super.onStart();
        mDatabaseManager.initGroups();
        mDatabaseManager.initMeeting();
        mDatabaseManager.setDatabaseManagerListener(this);
        mGoogleApiClient.connect();
    }

    @Override
    protected int getCurrentItemId() {
        return R.id.item_calendar;
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        verificationInternet();

        initGoogleMapClient();
        initViewAndLists();
        mDatabaseManager.initProfile();
        mDatabaseManager.saveCurrentUserDetails();
        mDatabaseManager.subscribeToContactsUpdates(this);
    }


    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        mDatabaseManager.setDatabaseManagerListener(null);
        super.onStop();
    }

    private void initGoogleMapClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
    }

    private void initViewAndLists() {
        Drawable calIcon = AppCompatResources.getDrawable(this, R.drawable.ic_date_range_black_24dp);
        mDateTimeEt.setCompoundDrawablesWithIntrinsicBounds(null, null, calIcon, null);
        mDurationEt.setCompoundDrawablesWithIntrinsicBounds(null, null, calIcon, null);
        Drawable locationIcon = AppCompatResources.getDrawable(this, R.drawable.ic_location_on_black_24dp);
        mLocationEt.setCompoundDrawablesWithIntrinsicBounds(null, null, locationIcon, null);
        Drawable reminderIcon = AppCompatResources.getDrawable(this, R.drawable.ic_notifications_black_24dp);
        mRemiderEt.setCompoundDrawablesWithIntrinsicBounds(null, null, reminderIcon, null);
        mListContactsModels = new ArrayList<>();
        mListContactsPhone = new ArrayList<>();
        mListParticipant = new ArrayList<>();
        mGroupsList = new ArrayList<>();
        mAutocompleteList = new ArrayList<>();
        mAdapterContacts = new ContactsAdapter(this,
                android.R.layout.simple_spinner_item, mAutocompleteList);

        mContactsEt.setAdapter(mAdapterContacts);
        mContactsEt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mContactsEt.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mContactsEt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mContactsEt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AutocompleteItemContact item = (AutocompleteItemContact) parent.getItemAtPosition(position);
                if (item.getContacts().size() == 1) {
                    //handle signle contact
                    Contact contact = item.getContacts().get(0);
                    if (!pickedContacts.contains(contact)) {
                        pickedContacts.add(contact);
                    }
                } else {
                    //handle group
                    for (UserGroup groupItem : mGroupsList) {
                        if (groupItem.getName().equals(item.getName()) && !pickedGroups.contains(groupItem)) {
                            pickedGroups.add(groupItem);
                            break;
                        }
                    }
                }
            }
        });
        mContactsEt.addTextChangedListener(mTextWatcher);
        initContactsList();
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mHandler.removeCallbacksAndMessages(null);
            if (s.length() > 0) {
                if (before > count && s.charAt(s.length() - 1) == ',') {
                    String temp = mContactsEt.getText().toString();
                    String[] usersFromInput = temp.replace(", ", ",").split(",");
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < usersFromInput.length - 1; i++) {
                        stringBuilder.append(usersFromInput[i]).append(", ");
                    }
                    mContactsEt.setText(stringBuilder.toString());
                }
                if (s.charAt(s.length() - 1) == ',' || (s.length() > 2 && s.charAt(s.length() - 2) == ',')) {
                    mHandler.postDelayed(mTextFilterRunnable, 50);
                }
            } else {
                pickedContacts.clear();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void applyFilter() {
        mContactsEt.removeTextChangedListener(mTextWatcher);
        Set<String> hashSet = new HashSet<>();
        HashSet<Contact> filteredContacts = new HashSet<>();
        HashSet<UserGroup> filteredGroups = new HashSet<>();

        String temp = mContactsEt.getText().toString();
        String[] usersFromInput = temp.replace(", ", ",").split(",");

        for (String phone : usersFromInput) {
            if (phone.isEmpty()) {
                continue;
            }
            Contact contact = new Contact("", phone);
            if (mListContactsPhone.contains(contact)) {
                filteredContacts.add(mListContactsPhone.get(mListContactsPhone.indexOf(contact)));
            } else {
                UserGroup group = new UserGroup(phone, null, null);
                if (mGroupsList.contains(group)) {
                    filteredGroups.add(mGroupsList.get(mGroupsList.indexOf(group)));
                } else {
                    hashSet.add(phone);
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder();

        String checkForRemoved = temp;

        for (Contact item : pickedContacts) {
            if (checkForRemoved.contains(item.getName())) {
                filteredContacts.add(item);
                checkForRemoved = checkForRemoved.replace(item.getName(), "");
            }
        }
        pickedContacts.clear();
        pickedContacts.addAll(filteredContacts);
        pickedGroups.clear();
        pickedGroups.addAll(filteredGroups);

        ArrayList<Contact> tempList = new ArrayList<>(pickedContacts);

        //add picked groups
        for (UserGroup item : pickedGroups) {
            stringBuilder.append(item.getName()).append(", ");
            for (Contact user : pickedContacts) {
                if (item.getUsers().contains(user)) {
                    tempList.remove(user);
                }
            }
        }

        pickedContacts.clear();
        pickedContacts.addAll(tempList);

        //add picked contacts
        for (Contact item : pickedContacts) {
            stringBuilder.append(item.getName()).append(", ");
        }

        //add user entered phone numbers
        for (String phone : hashSet) {
            if (TextUtils.isDigitsOnly(phone.replace("+", "")) && !stringBuilder.toString().contains(phone)) {
                stringBuilder.append(phone).append(", ");
            }
        }
        String prepared = stringBuilder.toString();

        //create spannable for colorifying
        Spannable spannable = new SpannableString(prepared);
        for (UserGroup userGroup : pickedGroups) {
            int startPosition = prepared.indexOf(userGroup.getName());
            int endPosition = startPosition + userGroup.getName().length();
            spannable.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.colorDodgerblue)), startPosition, endPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        for (Contact contact : pickedContacts) {
            int startPosition = prepared.indexOf(contact.getName());
            int endPosition = startPosition + contact.getName().length();

            ContactsModels contactsModels = new ContactsModels();
            contactsModels.setPhone(contact.getPhone());
            if (mListContactsModels.contains(contactsModels)) {
                spannable.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.colorOrange)), startPosition, endPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                spannable.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.ef_grey)), startPosition, endPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        mContactsEt.setText(spannable);
        mContactsEt.setSelection(mContactsEt.getText().length());
        mContactsEt.addTextChangedListener(mTextWatcher);
    }

    private Runnable mTextFilterRunnable = new Runnable() {
        @Override
        public void run() {
            applyFilter();
        }
    };

    private boolean createGroupContacts() {
        Set<String> hashSet = new HashSet<>();
        String temp = mContactsEt.getText().toString();

        String[] numbers = temp.split(", ");

        mListParticipant.clear();
        HashSet<Participant> filtered = new HashSet<>();
        mListParticipant.add(new Participant(mDatabaseManager.getProfile().getProfileName(), mSharedPreferences.getString(KEY_PHONE, DEFAULT), Constants.STATUS_CONFIDMED));
        for (Contact contact : pickedContacts) {
            filtered.add(new Participant(contact.getName(), contact.getPhone(), Constants.STATUS_PENDING));
        }
        for (UserGroup group : pickedGroups) {
            for (Contact contact : group.getUsers()) {
                filtered.add(new Participant(contact.getName(), contact.getPhone(), Constants.STATUS_PENDING));
            }
        }
        mListParticipant.addAll(filtered);
        for (String phone : numbers) {
            if (TextUtils.isDigitsOnly(phone.replace("+", ""))) {
                hashSet.add(phone);
            }
        }

        String currentPhoneNumber = mSharedPreferences.getString(KEY_PHONE, DEFAULT);
        if (hashSet.contains(currentPhoneNumber)) {
            return true;
        }
        ArrayList<String> memberList = new ArrayList<>(hashSet);
        Phonenumber.PhoneNumber swissNumberProto = null;
        for (int i = 0; i < memberList.size(); i++) {
            try {
                swissNumberProto = PhoneNumberUtil.getInstance().parse(memberList.get(i), Constants.DEFAULT_REGION);
            } catch (NumberParseException e) {
                e.printStackTrace();
            }
            try {
                if (!PhoneNumberUtil.getInstance().isValidNumber(swissNumberProto)) {
                    return true;
                } else
                    mListParticipant.add(new Participant(memberList.get(i), Constants.STATUS_PENDING));
            } catch (Exception e) {
                return true;
            }
        }
        return false;
    }

    public void initContactsList() {
        HashSet<Contact> contactsSet = new HashSet<>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds
                .Phone.CONTENT_URI, null, null, null, null);
        try {
            while (phones.moveToNext()) {
                contactsSet.add(new Contact(phones.getString(phones
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)),
                        phones.getString(phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))));
            }
            phones.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        mListContactsPhone.clear();
        mListContactsPhone.addAll(contactsSet);
        refillAutocomplete();
    }

    private void refillAutocomplete() {
        mAutocompleteList.clear();
        for (Contact contact : mListContactsPhone) {
            mAutocompleteList.add(new AutocompleteItemContact(contact));
        }
        for (UserGroup group : mGroupsList) {
            mAutocompleteList.add(new AutocompleteItemContact(group));
        }
        mAdapterContacts.notifyDataSetChanged();
    }

    @OnFocusChange(R.id.location_et)
    public void onClickLocation(View view, boolean isFocused) {
        if (isFocused) {
            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(this);
                startActivityForResult(intent, 1);
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.location_et)
    public void onClickLocation(View view) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.pick_contacts_btn)
    public void onClickPeekContacts(View view) {
        Intent intent = new Intent(this, PickContactActivity.class);
        ArrayList<Contact> pickedList = new ArrayList();
        pickedList.addAll(pickedContacts);
        intent.putParcelableArrayListExtra(Constants.KEY_CONTACTS, pickedList);
        startActivityForResult(intent, CONTACT_PICKER_RESULT);
        mContactsEt.requestFocus();
        mContactsEt.setSelection(mContactsEt.getText().length());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT: {
                    ArrayList<Contact> contacts = data.getParcelableArrayListExtra(KEY_CONTACTS);
                    pickedContacts.addAll(contacts);
                    StringBuilder stringBuilder = new StringBuilder(mContactsEt.getText().toString());
                    for (Contact contact : contacts) {
                        stringBuilder.append(contact.getName()).append(", ");
                    }
                    mContactsEt.setText(stringBuilder.toString());
                    applyFilter();
                    break;
                }
                case PLACE_PICKER_REQUEST: {
                    Place place = PlacePicker.getPlace(this, data);
                    mLocationEt.setText(place.getAddress());
                    mLatitude = (float) place.getLatLng().latitude;
                    mLongitude = (float) place.getLatLng().longitude;
                    break;
                }
            }
        } else {
            Log.d(TAG, "Pick contact failed");
        }
    }

    private void showDatePickerFragment(long minDate) {
        if (minDate == 0)
            minDate = System.currentTimeMillis();
        DatePickerFragmentDialog.newInstance(
                DateTimeBuilder.newInstance()
                        .withTime(true)
                        .with24Hours(true)
                        .withSelectedDate(System.currentTimeMillis())
                        .withMinDate(minDate)
                        .withTheme(R.style.PickersTheme))
                .show(getSupportFragmentManager(), "StartDatePickerFragmentDialog");
    }

    @OnFocusChange(R.id.datetime_et)
    public void onClickDataTime(View view, boolean isFocused) {
        if (isFocused) {
            mFirstAlwaysDate = 1;
            showDatePickerFragment(System.currentTimeMillis());
        }
    }

    @OnClick(R.id.datetime_et)
    public void onClickDataTime(View view) {
        mFirstAlwaysDate = 1;
        showDatePickerFragment(System.currentTimeMillis());
    }

    @OnFocusChange(R.id.duration_et)
    public void onClickDuration(View view, boolean isFocused) {
        if (isFocused) {
            mFirstAlwaysDate = 2;
            showDatePickerFragment(mStartTime);
        }
    }

    @OnClick(R.id.duration_et)
    public void onClickDuration(View view) {
        mFirstAlwaysDate = 2;
        showDatePickerFragment(mStartTime);
    }

    @OnClick(R.id.btnCreateMeeting)
    public void onClickOk(View view) {
        applyFilter();
        String meetingTopic = mMeetingTopicEt.getText().toString();
        if (meetingTopic.isEmpty()) {
            showMessage(R.string.error_meeting_empty);
            return;
        }
        String contacts = mContactsEt.getText().toString();
        if (contacts.isEmpty()) {
            showMessage(R.string.error_contact_empty);
            return;
        }
        String date = mDateTimeEt.getText().toString();
        if (date.isEmpty()) {
            showMessage(R.string.error_start_empty);
            return;
        }
        String duration = mDurationEt.getText().toString();
        if (duration.isEmpty()) {
            showMessage(R.string.error_stop_empty);
            return;
        }
        if (mStartTime > mStopTime) {
            showMessage(R.string.error_start_time_before_end);
        }

        if (mStopTime - mStartTime < 5 * 60 * 1000) { //5 mins
            showMessage(R.string.error_min_time);
            return;
        }

        if (new Date().getTime() > mStartTime) {
            showMessage(R.string.start_time_passed);
            return;
        }

        if (createGroupContacts()) {
            showMessage(R.string.error_invalid_format);
            return;
        }

        if (Integer.parseInt(mRemiderEt.getText().toString()) > 60) {
            showMessage(R.string.error_max_time);
            return;
        }

        if (!checkTimingsConflictForGroups()) {
            showMessage(R.string.group_time_error);
            return;
        }

        if (!checkConflictWithOtherMeetings()) {
            showMessage(R.string.conflict_time_error);
            return;
        }


        List<String> groups = new ArrayList<>();
        for (UserGroup group : pickedGroups) {
            groups.add(group.getName());
        }
        searchNewUsers();
        MeetingModel meetingModel = new MeetingModel(
                mSharedPreferences.getString(KEY_PHONE, DEFAULT),
                mMeetingTopicEt.getText().toString(),
                mStartTime,
                mStopTime,
                mLocationEt.getText().toString(),
                mLatitude,
                mLongitude,
                (mRemiderEt.getText().toString().equals("")) ? "0" : mRemiderEt.getText().toString(),
                mListParticipant,
                groups);

        String nameMeeting = mMeetingTopicEt.getText().toString().trim();
        if (!mRemiderEt.getText().toString().isEmpty()) {
            final String timeString = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(mStartTime);
            String scheduledAt = mMeetingTopicEt.getText().toString() + "\n at -" + timeString;
            scheduleNotification(this, scheduledAt, nameMeeting, mStartTime, Integer.parseInt(mRemiderEt.getText().toString()));
        }
        //send
        mDatabaseManager.addOrUpdateMeeting(meetingModel, nameMeeting);

        startActivity(new Intent(CreateMeetingActivity.this, CalendarActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        Toast.makeText(this, getString(R.string.meeting_created), Toast.LENGTH_SHORT).show();
    }

    private boolean checkConflictWithOtherMeetings() {
        DateTime startDateTime = new DateTime(mStartTime);
        startDateTime = startDateTime.withSecondOfMinute(0).withMillisOfSecond(0);
        DateTime endtDateTime = new DateTime(mStopTime);
        endtDateTime = endtDateTime.withSecondOfMinute(0).withMillisOfSecond(0);

        MeetingModel meetingModel = new MeetingModel();
        meetingModel.setAuthor(mSharedPreferences.getString(KEY_PHONE, DEFAULT));
        meetingModel.setTopic(mMeetingTopicEt.getText().toString());
        meetingModel.setDate(startDateTime.getMillis());
        meetingModel.setDuration(endtDateTime.getMillis());
        meetingModel.setParticipants(mListParticipant);
        return TimeUtil.isMeetingsMatched(meetingModel, mDatabaseManager.getMeetingModels());
    }

    private boolean checkTimingsConflictForGroups() {
        return TimeUtil.isGroupsMatched(mStartTime, mStopTime, pickedGroups);
    }


    @OnClick(R.id.btnCancel)
    public void onClick(View view) {
        finish();
    }

    private void searchNewUsers() {
        for (int i = 0; i < mListParticipant.size(); i++) {
            int count = 0;
            for (int j = 0; j < mListContactsModels.size(); j++) {
                if (!mListParticipant.get(i).getPhone().equals(mListContactsModels.get(j).getPhone())) {
                    count++;
                } else {
                    count--;
                }
                if (count == mListContactsModels.size()) {
                    mRestController.inviteToMeeting(this, mListParticipant.get(i).getPhone(), mMeetingTopicEt.getText().toString());
                }
            }
        }
    }


    private void verificationInternet() {
        if (!Utils.isNetworkOnline(this)) {
            Toast.makeText(this, getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.home_iv)
    public void onClickHome(View view) {
        finish();
    }

    @Override
    public void onDateSet(long date) {
    }

    @Override
    public void onTimeSet(long timeOnly, long dateWithTime) {
        if (mFirstAlwaysDate == 1) {
            mStartTime = new DateTime(dateWithTime).withSecondOfMinute(0).withMillisOfSecond(0).getMillis();
            if (dateWithTime < System.currentTimeMillis()) {
                Toast.makeText(this, getString(R.string.error_time_passed), Toast.LENGTH_SHORT).show();
                return;
            }
            mDateTimeEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(dateWithTime));
            mFirstAlwaysDate = 0;
        }
        if (mFirstAlwaysDate == 2) {
            if (mStartTime < dateWithTime) {
                mStopTime = new DateTime(dateWithTime).withSecondOfMinute(0).withMillisOfSecond(0).getMillis();
                ;
                if (dateWithTime < System.currentTimeMillis() || dateWithTime < mStartTime) {
                    Toast.makeText(this, getString(R.string.error_time_passed), Toast.LENGTH_SHORT).show();
                    return;
                }
                mDurationEt.setText(new SimpleDateFormat(DATE_FORMAT, ENGLISH).format(dateWithTime));
                mFirstAlwaysDate = 0;
            } else {
                Toast.makeText(this, getString(R.string.error_start_time_before_end), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, connectionResult.getErrorMessage());
    }

    @Override
    public void onDataChanged(String url, DataSnapshot dataSnapshot) {
        switch (url) {
            case URL_CONTACTS:
                mListContactsModels.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    ContactsModels contactsModels = noteDataSnapshot.getValue(ContactsModels.class);
                    mListContactsModels.add(contactsModels);
                }
                break;
            case URL_GROUPS:
                mGroupsList.clear();
                mGroupsList.addAll(mDatabaseManager.getListUserGroups());
                refillAutocomplete();
                break;
        }
    }

    @Override
    public void onCancelled(DatabaseError error) {

    }


}