package kinect.pro.meetingapp.activity.base;

import android.app.ProgressDialog;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.MeetingModel;

/**
 * Created on 23.11.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.root_view)
    View mRootView;
    protected Snackbar mSnackbar;
    ProgressDialog mProgressDialog;

    protected void showMessage(@StringRes int errorResId) {
        showMessage(getString(errorResId));
    }

    protected void showMessage(String error) {
        if (mRootView == null) {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        } else {
            mSnackbar = Snackbar.make(mRootView, error, Snackbar.LENGTH_LONG)
                    .setAction("Dismiss", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSnackbar.dismiss();
                        }
                    });
            mSnackbar.setDuration(2500);
            mSnackbar.show();
        }
    }

    public void showSnackBar(String text) {

    }

    @Optional
    @OnClick(R.id.drawer_iv)
    public void onClickHome(View view) {
        onBackPressed();
    }

    public void showProgress(final boolean show) {
        if (mProgressDialog == null) {
            mProgressDialog = createProgressDialog();
        }

        if (!mProgressDialog.isShowing() && show) {
            mProgressDialog.show();
        } else if (mProgressDialog.isShowing() && !show) {
            mProgressDialog.dismiss();
        }
    }

    private ProgressDialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.loading_data));
        dialog.setTitle("");
        dialog.setCancelable(true);
        return dialog;
    }
}
