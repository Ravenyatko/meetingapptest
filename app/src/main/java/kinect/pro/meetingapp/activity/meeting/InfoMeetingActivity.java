package kinect.pro.meetingapp.activity.meeting;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import kinect.pro.meetingapp.App;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.activity.base.BaseMenuActivity;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.other.Constants;
import kinect.pro.meetingapp.other.Utils;

import static kinect.pro.meetingapp.other.Constants.DATE_FORMAT;
import static kinect.pro.meetingapp.other.Constants.DEFAULT;
import static kinect.pro.meetingapp.other.Constants.KEY_INFO_EVENT;
import static kinect.pro.meetingapp.other.Constants.KEY_PHONE;
import static kinect.pro.meetingapp.other.Constants.MEETING;
import static kinect.pro.meetingapp.other.Constants.STATUS_CANCELED;
import static kinect.pro.meetingapp.other.Constants.STATUS_CONFIDMED;
import static kinect.pro.meetingapp.other.Constants.URL_MEETING;
import static kinect.pro.meetingapp.popup.PopupAlarmManager.scheduleNotification;

public class InfoMeetingActivity extends BaseMenuActivity implements OnMapReadyCallback {

    public static final String TAG = "InfoMeetingActivity ->";

    @Inject
    SharedPreferences sharedPreferences;

    @BindView(R.id.meeting_topic_tv)
    TextView mMeetingTopicTv;
    @BindView(R.id.participant_tv)
    TextView mParticipantTv;
    @BindView(R.id.start_meeting_tv)
    TextView mStartMeetingTv;
    @BindView(R.id.stop_meeting_tv)
    TextView mStopMeetingTv;
    @BindView(R.id.locations_tv)
    TextView mLocationsTv;
    @BindView(R.id.map_view)
    LinearLayout mMapLL;
    @BindView(R.id.confirmed_btn)
    Button mConfirmedBtn;
    @BindView(R.id.cancelled_btn)
    Button mCancelledBtn;

    private DatabaseReference mDatabaseReference;
    private FirebaseDatabase mDatabase;
    private MeetingModel mMeetingModel;
    private GoogleMap mMap;
    private String mMyPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        mDatabase = FirebaseDatabase.getInstance();
        initMapFragment();
        mMyPhoneNumber = sharedPreferences.getString(KEY_PHONE, DEFAULT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Gson gson = new Gson();
        MeetingModel item = gson.fromJson(sharedPreferences.getString(KEY_INFO_EVENT, null), MeetingModel.class);
        initFirebase(item);
    }

    @Override
    protected int getCurrentItemId() {
        return R.id.item_calendar;
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_info_meeting;
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (mapFragment.getView() != null) {
            mapFragment.getView().setClickable(false);
        }
    }

    private void initFirebase(MeetingModel meetingModel) {
        mDatabaseReference = mDatabase.getReference(URL_MEETING);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    MeetingModel mModels = noteDataSnapshot.getValue(MeetingModel.class);
                    if (mModels != null) {
                        if (mModels.getTopic().equals(meetingModel.getTopic()) &&
                                mModels.getAuthor().equals(meetingModel.getAuthor())
                                && mModels.getDate() == meetingModel.getDate()) {
                            if (mModels.getParticipants().get(0).getStatus() != null) {
                                mMeetingModel = mModels;
                                initView(mMeetingModel);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
    }

    private void initView(MeetingModel meetingModel) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        mParticipantTv.setText(Utils.buildParticipantsString(meetingModel));
        mMeetingTopicTv.setText(meetingModel.getTopic());
        mStartMeetingTv.setText(String.format("%s - %s", getResources().getText(R.string.start_time), dateFormat.format(meetingModel.getDate())));
        mStopMeetingTv.setText(String.format("%s - %s", getResources().getText(R.string.stop_time), dateFormat.format(meetingModel.getDuration())));
        mLocationsTv.setText(meetingModel.getLocation());
        initMap(meetingModel.getLatitude(), meetingModel.getLongitude());
        initBtnBottom(meetingModel);
    }

    public void initBtnBottom(MeetingModel meetingModel) {
        for (int i = 0; i < meetingModel.getParticipants().size(); i++) {
            if (meetingModel.getParticipants().get(i).getPhone()
                    .equals(mMyPhoneNumber)) {
                if (!meetingModel.getParticipants().get(i).getStatus().equals(Constants.STATUS_PENDING)) {
                    mConfirmedBtn.setVisibility(View.INVISIBLE);
                    mCancelledBtn.setVisibility(View.INVISIBLE);
                } else {
                    mConfirmedBtn.setVisibility(View.VISIBLE);
                    mCancelledBtn.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void initMap(float latitude, float longitude) {
        if (latitude != 0L && longitude != 0L) {
            LatLng meetingMarker = new LatLng(mMeetingModel.getLatitude(), mMeetingModel.getLongitude());
            mMap.addMarker(new MarkerOptions().position(meetingMarker).title(MEETING));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(meetingMarker, 15));
        } else {
            mMapLL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @OnClick(R.id.cancelled_btn)
    public void btnCanceled(View view) {
        for (int i = 0; i < mMeetingModel.getParticipants().size(); i++) {
            if (mMeetingModel.getParticipants().get(i).getPhone().equals(mMyPhoneNumber)) {
                String nameMeeting = mMeetingModel.getTopic().trim();
                mDatabaseReference = mDatabase.getReference(URL_MEETING + nameMeeting);
                mMeetingModel.getParticipants().get(i).setStatus(STATUS_CANCELED);
                mDatabaseReference.setValue(mMeetingModel);
                finish();
            }
        }
    }

    @OnClick(R.id.confirmed_btn)
    public void btnConfirmed(View view) {
        for (int i = 0; i < mMeetingModel.getParticipants().size(); i++) {
            if (mMeetingModel.getParticipants().get(i).getPhone().equals(mMyPhoneNumber)) {
                String nameMeeting = mMeetingModel.getTopic().trim();
                mDatabaseReference = mDatabase.getReference(URL_MEETING + nameMeeting);
                mMeetingModel.getParticipants().get(i).setStatus(STATUS_CONFIDMED);
                mDatabaseReference.setValue(mMeetingModel);
                scheduleNotificationApp(getApplicationContext(), mMeetingModel);
                finish();
            }
        }
    }

    @OnClick(R.id.change_date_btn)
    public void changeDate(View view) {
        Intent intent = new Intent(this, ChangeDateActivity.class);
        startActivity(intent);
    }

    private static void scheduleNotificationApp(Context context, MeetingModel meetingModel) {
        String nameMeeting = meetingModel.getTopic().trim();
        if (System.currentTimeMillis() < meetingModel.getDate()) {
            final String timeString = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(meetingModel.getDate());
            String scheduledAt = meetingModel.getTopic() + "\n at -" + timeString;
            scheduleNotification(context, scheduledAt, nameMeeting, meetingModel.getDate(), Integer.parseInt(meetingModel.getReminder()));
        }
    }

    public void onClickHome(View view) {
        onBackPressed();
    }
}

