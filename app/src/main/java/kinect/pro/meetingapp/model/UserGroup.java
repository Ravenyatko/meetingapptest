package kinect.pro.meetingapp.model;

import java.util.List;

/**
 * Created on 01.12.2017.
 */

public class UserGroup {

    private String name;
    private List<WeekDayTimeModel> availableTimes = null;
    private List<Contact> users = null;

    public UserGroup() {
    }

    public UserGroup(String name, List<WeekDayTimeModel> availableTimes, List<Contact> users) {
        this.availableTimes = availableTimes;
        this.users = users;
        this.name = name;
    }

    public List<WeekDayTimeModel> getAvailableTimes() {
        return availableTimes;
    }

    public void setAvailableTimes(List<WeekDayTimeModel> availableTimes) {
        this.availableTimes = availableTimes;
    }

    public List<Contact> getUsers() {
        return users;
    }

    public void setUsers(List<Contact> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserGroup) {
            return ((UserGroup) obj).getName().equals(name);
        } else {
            return false;
        }
    }
}
