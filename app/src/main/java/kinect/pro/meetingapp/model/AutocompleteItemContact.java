package kinect.pro.meetingapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 01.12.2017.
 */

public class AutocompleteItemContact {
    String name;
    List<Contact> contacts = new ArrayList<>();

    public AutocompleteItemContact(Contact contact) {
        name = "";
        contacts.add(contact);
    }

    public AutocompleteItemContact(UserGroup group) {
        name = group.getName();
        contacts.addAll(group.getUsers());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        if(contacts.size() == 1){
            return contacts.get(0).getName();
        } else {
            return name;
        }
    }
}