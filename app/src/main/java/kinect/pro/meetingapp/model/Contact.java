package kinect.pro.meetingapp.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

public class Contact implements Parcelable {

    private String name;
    private String phone;
    @Exclude
    private Uri photo;

    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone.replace(" ", "");
    }

    public Contact(String name, String phone, Uri photo) {
        this(name, phone);
        this.photo = photo;
    }

    public Contact() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone.replace(" ", "");
    }

    @Exclude
    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return phone.hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeParcelable(this.photo, flags);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Contact) {
            return ((Contact) obj).getPhone().equals(phone) && ((Contact) obj).getName().equals(name);
        } else {
            return super.equals(obj);
        }
    }

    protected Contact(Parcel in) {
        this.name = in.readString();
        this.phone = in.readString();
        this.photo = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
}
