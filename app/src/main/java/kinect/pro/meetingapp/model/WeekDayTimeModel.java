package kinect.pro.meetingapp.model;

import org.joda.time.Interval;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import kinect.pro.meetingapp.other.Constants;

public class WeekDayTimeModel {
    int dayOfWeek;
    String startTime;
    String endTime;

    public WeekDayTimeModel() {
    }

    public WeekDayTimeModel(int dayOfWeek, String startTime, String endTime) {
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WeekDayTimeModel && ((WeekDayTimeModel) obj).dayOfWeek == dayOfWeek) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.TIME_FORMAT, Locale.ENGLISH);
            try {
                Interval currentInterval = new Interval(dateFormat.parse(startTime).getTime(), dateFormat.parse(endTime).getTime());
                Interval checkingInterval = new Interval(dateFormat.parse(((WeekDayTimeModel) obj).getStartTime()).getTime(),
                        dateFormat.parse(((WeekDayTimeModel) obj).getEndTime()).getTime());
                if (currentInterval.overlap(checkingInterval) == null) {
                    return false;
                } else {
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
}