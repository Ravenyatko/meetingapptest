package kinect.pro.meetingapp.model;


public class Participant {

    private String name;
    private String phone;
    private String status;

    public Participant(String name, String phone, String status) {
        this.name = name;
        this.phone = phone;
        this.status = status;
    }

    public Participant(String phone, String status) {
        this.phone = phone;
        this.status = status;
    }

    public Participant() {
    }

    @Override
    public int hashCode() {
        return phone.hashCode();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Participant){
            return ((Participant) obj).getPhone().equals(phone);
        } else {
            return false;
        }
    }
}
