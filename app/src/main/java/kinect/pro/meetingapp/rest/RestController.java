package kinect.pro.meetingapp.rest;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import kinect.pro.meetingapp.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static kinect.pro.meetingapp.other.Constants.ACCOUNT_SID;
import static kinect.pro.meetingapp.other.Constants.AUTH_TOKEN;
import static kinect.pro.meetingapp.other.Constants.NUMBER_FROM;


public class RestController {

    private static final String TAG = RestController.class.getSimpleName();

    private TwilioApi mTwillioApi;

    @Inject
    public RestController(TwilioApi twilioApi) {
        mTwillioApi = twilioApi;
    }

    public void inviteToMeeting(Context context, String phone, String event) {
        String body = String.format(context.getString(R.string.invite_message_formatter), event);

        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                (ACCOUNT_SID + ":" + AUTH_TOKEN).getBytes(), Base64.NO_WRAP
        );
        Map<String, String> data = new HashMap<>();
        data.put("From", NUMBER_FROM);
        data.put("To", phone);
        data.put("Body", body);

        mTwillioApi.sendMessage(ACCOUNT_SID, base64EncodedCredentials, data).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse->success");
                    Toast.makeText(context, context.getString(R.string.sms_sent_sucess), Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, response.message());
                    Toast.makeText(context, context.getString(R.string.sms_sent_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure");
            }
        });
    }

    public void notifyTimeChanged(Context context, String phone, String event, String startTime, String endTime) {
        String body = String.format(context.getString(R.string.time_changed_formatter), event, startTime, endTime);

        String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                (ACCOUNT_SID + ":" + AUTH_TOKEN).getBytes(), Base64.NO_WRAP
        );
        Map<String, String> data = new HashMap<>();
        data.put("From", NUMBER_FROM);
        data.put("To", phone);
        data.put("Body", body);

        mTwillioApi.sendMessage(ACCOUNT_SID, base64EncodedCredentials, data).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse->success");
                } else {
                    Log.d(TAG, response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure");
            }
        });
    }


}
