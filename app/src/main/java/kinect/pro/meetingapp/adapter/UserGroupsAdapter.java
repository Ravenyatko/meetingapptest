package kinect.pro.meetingapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.UserGroup;

/**
 * Created on 01.12.2017.
 */

public class UserGroupsAdapter extends RecyclerView.Adapter<UserGroupsAdapter.GroupVH> {

    private List<UserGroup> groups;

    public UserGroupsAdapter(List<UserGroup> groups) {
        this.groups = groups;
    }

    @Override
    public GroupVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_group, parent, false));
    }

    @Override
    public void onBindViewHolder(GroupVH holder, int position) {
        holder.bind(groups.get(position));
    }

    @Override
    public int getItemCount() {
        return groups == null ? 0 : groups.size();
    }

    class GroupVH extends RecyclerView.ViewHolder {

        @BindView(R.id.group_tv)
        TextView groupTv;

        public GroupVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(UserGroup group) {
            groupTv.setText(group.getName());
        }
    }

}
