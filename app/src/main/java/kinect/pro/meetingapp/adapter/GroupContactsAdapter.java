package kinect.pro.meetingapp.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.Contact;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.UserGroup;

/**
 * Created on 23.11.2017.
 */

public class GroupContactsAdapter extends RecyclerView.Adapter<GroupContactsAdapter.ContactVH> {

    private ArrayList<Contact> mPicked = new ArrayList<>();
    private ArrayList<ContactsModels> mListContactsModels;

    public GroupContactsAdapter(ArrayList<Contact> mPicked, ArrayList<ContactsModels> mListContactsModels) {
        this.mPicked = mPicked;
        this.mListContactsModels = mListContactsModels;
    }

    @Override
    public ContactVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactVH holder, int position) {
        holder.bind(mPicked.get(position));
    }

    @Override
    public int getItemCount() {
        return mPicked == null ? 0 : mPicked.size();
    }

    class ContactVH extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_ll)
        View contactLl;
        @BindView(R.id.user_name_tv)
        TextView userNameTv;
        @BindView(R.id.user_phone_tv)
        TextView userPhoneTv;


        public ContactVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Contact item) {
            userNameTv.setText(item.getName());
            userPhoneTv.setText(item.getPhone());
            setBGcolor(item);
        }

        private void setBGcolor(Contact item) {
            ContactsModels contactsModels = new ContactsModels();
            contactsModels.setPhone(item.getPhone());
            if (mListContactsModels.contains(contactsModels)) {
                contactLl.setBackgroundColor(itemView.getResources().getColor(R.color.colorOrange));
            } else {
                contactLl.setBackgroundColor(itemView.getResources().getColor(R.color.ef_grey));
            }
        }
    }

}
