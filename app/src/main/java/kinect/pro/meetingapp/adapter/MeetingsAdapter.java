package kinect.pro.meetingapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.MeetingModel;
import kinect.pro.meetingapp.model.Participant;
import kinect.pro.meetingapp.other.Constants;
import kinect.pro.meetingapp.other.Utils;
import okhttp3.internal.Util;

import static kinect.pro.meetingapp.other.Constants.DATE_FORMAT;

/**
 * Created on 23.11.2017.
 */

public class MeetingsAdapter extends RecyclerView.Adapter<MeetingsAdapter.MeetingVH> {

    private List<MeetingModel> mMeetingModels;
    private OnMeetingClickListener mOnMeetingClickListener;
    private String currentUserPhone;

    public MeetingsAdapter(List<MeetingModel> meetingModels, String currentUSerPhone) {
        this.mMeetingModels = meetingModels;
        this.currentUserPhone = currentUSerPhone;
    }

    public void setOnMeetingClickListener(OnMeetingClickListener onMeetingClickListener) {
        this.mOnMeetingClickListener = onMeetingClickListener;
    }

    @Override
    public MeetingVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MeetingVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_meeting, parent, false));
    }

    @Override
    public void onBindViewHolder(MeetingVH holder, int position) {
        holder.bind(mMeetingModels.get(position));
    }

    @Override
    public int getItemCount() {
        return mMeetingModels == null ? 0 : mMeetingModels.size();
    }

    class MeetingVH extends RecyclerView.ViewHolder {

        @BindView(R.id.meeting_name_tv)
        TextView meetingNameTv;
        @BindView(R.id.parent_ll)
        View parentLL;
        @BindView(R.id.contacts_tv)
        TextView contactsTv;
        @BindView(R.id.start_date_tv)
        TextView startTv;
        @BindView(R.id.stop_date_tv)
        TextView stopTv;

        public MeetingVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(MeetingModel item) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
            meetingNameTv.setText(item.getTopic());
            contactsTv.setText(Utils.buildParticipantsString(item));
            startTv.setText(dateFormat.format(item.getDate()));
            stopTv.setText(dateFormat.format(item.getDuration()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnMeetingClickListener != null) {
                        mOnMeetingClickListener.onMeetingClick(item);
                    }
                }
            });
            parentLL.setBackgroundColor(itemView.getResources().getColor(Utils.getMeetingColor(item)));
        }
    }

    public interface OnMeetingClickListener {
        void onMeetingClick(MeetingModel meeting);
    }
}
