package kinect.pro.meetingapp.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kinect.pro.meetingapp.R;
import kinect.pro.meetingapp.model.Contact;
import kinect.pro.meetingapp.model.ContactsModels;
import kinect.pro.meetingapp.model.UserGroup;
import kinect.pro.meetingapp.other.CircleTransform;

/**
 * Created on 23.11.2017.
 */

public class PickContactsAdapter extends RecyclerView.Adapter<PickContactsAdapter.ContactVH> {

    private ArrayList<Contact> mPicked = new ArrayList<>();
    private List<Contact> mContacts;
    private ArrayList<ContactsModels> mListContactsModels;
    private OnPickedChangeListener onPickedChangeListener;
    private ArrayList<UserGroup> mGroupsList = new ArrayList<>();

    public PickContactsAdapter(List<Contact> mContacts, ArrayList<ContactsModels> inappContacts, ArrayList<UserGroup> groupsList) {
        this.mContacts = mContacts;
        this.mListContactsModels = inappContacts;
        this.mGroupsList = groupsList;
    }

    @Override
    public ContactVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactVH holder, int position) {
        holder.bind(mContacts.get(position));
    }

    @Override
    public int getItemCount() {
        return mContacts == null ? 0 : mContacts.size();
    }

    class ContactVH extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_ll)
        View contactLl;
        @BindView(R.id.user_photo_iv)
        ImageView userPhotoIv;
        @BindView(R.id.pick_iv)
        ImageView pickIv;
        @BindView(R.id.user_name_tv)
        TextView userNameTv;
        @BindView(R.id.user_phone_tv)
        TextView userPhoneTv;
        @BindView(R.id.groups_rv)
        RecyclerView groupsRv;

        public ContactVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            groupsRv.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        }

        public void bind(Contact item) {
            userNameTv.setText(item.getName());
            userPhoneTv.setText(item.getPhone());
            if (item.getPhoto() != null) {
                Picasso.with(itemView.getContext())
                        .load(item.getPhoto())
                        .placeholder(R.drawable.ic_people)
                        .transform(new CircleTransform())
                        .into(userPhotoIv);
            } else {
                userPhotoIv.setImageResource(R.drawable.ic_people);
            }
            initPickLogic(item);
            setBGcolor(item);
            ArrayList<UserGroup> groups = new ArrayList<>();
            for (UserGroup group : mGroupsList) {
                if(group.getUsers().contains(item)){
                    groups.add(group);
                }
            }
            groupsRv.setAdapter(new UserGroupsAdapter(groups));
        }

        private void initPickLogic(Contact item) {
            if (mPicked.contains(item)) {
                pickIv.setVisibility(View.VISIBLE);
            } else {
                pickIv.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPicked.contains(item)) {
                        mPicked.remove(item);
                    } else {
                        mPicked.add(item);
                    }
                    if (onPickedChangeListener != null) {
                        onPickedChangeListener.onPickedChanged(mPicked);
                    }
                    notifyDataSetChanged();
                }
            });
        }

        private void setBGcolor(Contact item) {
            ContactsModels contactsModels = new ContactsModels();
            contactsModels.setPhone(item.getPhone());
            if (mListContactsModels.contains(contactsModels)) {
                contactLl.setBackgroundColor(itemView.getResources().getColor(R.color.colorOrange));
            } else {
                contactLl.setBackgroundColor(itemView.getResources().getColor(R.color.ef_grey));
            }
        }
    }

    public void setPicked(ArrayList<Contact> mPicked) {
        this.mPicked = mPicked;
    }

    public ArrayList<Contact> getPicked() {
        return mPicked;
    }

    public void setOnPickedChangeListener(OnPickedChangeListener onPickedChangeListener) {
        this.onPickedChangeListener = onPickedChangeListener;
    }

    public interface OnPickedChangeListener {
        void onPickedChanged(ArrayList<Contact> picked);
    }
}
