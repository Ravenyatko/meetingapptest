package kinect.pro.meetingapp.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import kinect.pro.meetingapp.model.AutocompleteItemContact;
import kinect.pro.meetingapp.model.Contact;

public class ContactsAdapter extends ArrayAdapter {

    private ArrayList<AutocompleteItemContact> items;
    private int viewResourceId;

    private ArrayList<AutocompleteItemContact> filteredData = null;

    public ContactsAdapter(Context context, int viewResourceId, ArrayList<AutocompleteItemContact> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        ViewHolder holder;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = vi.inflate(viewResourceId, null);

            holder = new ViewHolder();
            holder.contact = v.findViewById(android.R.id.text1);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        try {
            AutocompleteItemContact contact = filteredData.get(position);
            if (contact.getName() == null || contact.getName().isEmpty() && contact.getContacts().size() > 0) {
                holder.contact.setText(String.format("%s - %s",
                        contact.getContacts().get(0).getPhone(),
                        contact.getContacts().get(0).getName()));
            } else {
                holder.contact.setText(contact.getName());
            }
        } catch (IndexOutOfBoundsException e) {
            holder.contact.setText("");
            e.printStackTrace();
        }
        return v;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public int getCount() {
        return (filteredData == null) ? items.size() : filteredData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return new ContactFilter();
    }

    private class ContactFilter extends Filter {

        private int lastConstraintSize = 0;

        @Override
        public String convertResultToString(Object resultValue) {
            return resultValue.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                ArrayList<AutocompleteItemContact> mSuggestions = new ArrayList<>();

                if (lastConstraintSize != 0 && lastConstraintSize < constraint.toString().length()) {
                    for (AutocompleteItemContact value : filteredData) {
                        addValue(constraint, mSuggestions, value);
                    }
                } else {
                    for (AutocompleteItemContact value : items) {
                        addValue(constraint, mSuggestions, value);
                    }
                }

                lastConstraintSize = constraint.toString().length();

                FilterResults filterResults = new FilterResults();
                filterResults.values = mSuggestions;
                filterResults.count = mSuggestions.size();

                return filterResults;
            } else {
                return null;
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                filteredData = (ArrayList<AutocompleteItemContact>) results.values;
                notifyDataSetChanged();
            }
        }
    }

    private void addValue(CharSequence constraint, ArrayList<AutocompleteItemContact> mSuggestions, AutocompleteItemContact value) {
        if (value.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
            mSuggestions.add(value);
        } else if (
                (value.getContacts().get(0).getPhone().length() >= constraint.length() && value.getContacts().get(0).getPhone().substring(0, constraint.toString().length())
                        .equals(constraint.toString())) ||
                (value.getContacts().get(0).getName().length() >= constraint.length() && value.getContacts().get(0).getName().substring(0, constraint.toString().length()).toLowerCase()
                        .contains(constraint.toString().toLowerCase()))) {
            mSuggestions.add(value);
        }
    }

    static class ViewHolder {
        TextView contact;
    }
}
